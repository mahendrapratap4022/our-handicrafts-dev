-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:05 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `our_handicrafts`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `booking_details` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_uuid` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visitor_uuid` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `start_form` double NOT NULL,
  `end_to` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_uuid` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `uuid`, `name`, `description`, `start_form`, `end_to`, `image`, `parent_uuid`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '9fa15667-c0f5-11e8-8b08-34e6d7235aff', 'test', 'test s wegew weg we gewg we', 150, 800, 'dad.jpg', NULL, 1, NULL, NULL, NULL),
(6, 'd6bac20a-ca98-434c-8546-3fe3b48b4e57', 'mahendradsdas', 'wqeqwe', 124, 2142134, 'mahendradsdas-5babd4000fffe.jpg', '9fa15667-c0f5-11e8-8b08-34e6d7235aff', 1, '2018-10-03 12:08:49', '2018-09-26 13:16:24', '2018-10-03 12:08:49'),
(7, 'f92ccf90-3b2c-4de4-b7b9-5faee787c665', 'test', 'test ewwes wegew weg we gewg we', 150, 800, NULL, 'null', 0, NULL, '2018-10-14 12:25:13', '2018-10-14 12:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `hexa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `hexa`, `name`, `created_at`, `updated_at`) VALUES
(3, '#000000', 'black', '2018-10-03 13:07:33', '2018-10-03 13:07:33'),
(4, '#ff1201', 'red', '2018-10-03 13:07:51', '2018-10-03 13:07:51');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Andorra', 'ad', NULL, NULL),
(2, 'United Arab Emirates', 'ae', NULL, NULL),
(3, 'Afghanistan', 'af', NULL, NULL),
(4, 'Antigua and Barbuda', 'ag', NULL, NULL),
(5, 'Anguilla', 'ai', NULL, NULL),
(6, 'Albania', 'al', NULL, NULL),
(7, 'Armenia', 'am', NULL, NULL),
(8, 'Netherlands Antilles', 'an', NULL, NULL),
(9, 'Angola', 'ao', NULL, NULL),
(10, 'Argentina', 'ar', NULL, NULL),
(11, 'Austria', 'at', NULL, NULL),
(12, 'Australia', 'au', NULL, NULL),
(13, 'Aruba', 'aw', NULL, NULL),
(14, 'Azerbaijan', 'az', NULL, NULL),
(15, 'Bosnia and Herzegovina', 'ba', NULL, NULL),
(16, 'Barbados', 'bb', NULL, NULL),
(17, 'Bangladesh', 'bd', NULL, NULL),
(18, 'Belgium', 'be', NULL, NULL),
(19, 'Burkina Faso', 'bf', NULL, NULL),
(20, 'Bulgaria', 'bg', NULL, NULL),
(21, 'Bahrain', 'bh', NULL, NULL),
(22, 'Burundi', 'bi', NULL, NULL),
(23, 'Benin', 'bj', NULL, NULL),
(24, 'Bermuda', 'bm', NULL, NULL),
(25, 'Brunei Darussalam', 'bn', NULL, NULL),
(26, 'Bolivia', 'bo', NULL, NULL),
(27, 'Brazil', 'br', NULL, NULL),
(28, 'Bahamas', 'bs', NULL, NULL),
(29, 'Bhutan', 'bt', NULL, NULL),
(30, 'Botswana', 'bw', NULL, NULL),
(31, 'Belarus', 'by', NULL, NULL),
(32, 'Belize', 'bz', NULL, NULL),
(33, 'Canada', 'ca', NULL, NULL),
(34, 'Cocos (Keeling) Islands', 'cc', NULL, NULL),
(35, 'Democratic Republic of the Congo', 'cd', NULL, NULL),
(36, 'Central African Republic', 'cf', NULL, NULL),
(37, 'Congo', 'cg', NULL, NULL),
(38, 'Switzerland', 'ch', NULL, NULL),
(39, 'Cote D\'Ivoire (Ivory Coast)', 'ci', NULL, NULL),
(40, 'Cook Islands', 'ck', NULL, NULL),
(41, 'Chile', 'cl', NULL, NULL),
(42, 'Cameroon', 'cm', NULL, NULL),
(43, 'China', 'cn', NULL, NULL),
(44, 'Colombia', 'co', NULL, NULL),
(45, 'Costa Rica', 'cr', NULL, NULL),
(46, 'Cuba', 'cu', NULL, NULL),
(47, 'Cape Verde', 'cv', NULL, NULL),
(48, 'Christmas Island', 'cx', NULL, NULL),
(49, 'Cyprus', 'cy', NULL, NULL),
(50, 'Czech Republic', 'cz', NULL, NULL),
(51, 'Germany', 'de', NULL, NULL),
(52, 'Djibouti', 'dj', NULL, NULL),
(53, 'Denmark', 'dk', NULL, NULL),
(54, 'Dominica', 'dm', NULL, NULL),
(55, 'Dominican Republic', 'do', NULL, NULL),
(56, 'Algeria', 'dz', NULL, NULL),
(57, 'Ecuador', 'ec', NULL, NULL),
(58, 'Estonia', 'ee', NULL, NULL),
(59, 'Egypt', 'eg', NULL, NULL),
(60, 'Western Sahara', 'eh', NULL, NULL),
(61, 'Eritrea', 'er', NULL, NULL),
(62, 'Spain', 'es', NULL, NULL),
(63, 'Ethiopia', 'et', NULL, NULL),
(64, 'Finland', 'fi', NULL, NULL),
(65, 'Fiji', 'fj', NULL, NULL),
(66, 'Falkland Islands (Malvinas)', 'fk', NULL, NULL),
(67, 'Federated States of Micronesia', 'fm', NULL, NULL),
(68, 'Faroe Islands', 'fo', NULL, NULL),
(69, 'France', 'fr', NULL, NULL),
(70, 'Gabon', 'ga', NULL, NULL),
(71, 'Great Britain (UK)', 'gb', NULL, NULL),
(72, 'Grenada', 'gd', NULL, NULL),
(73, 'Georgia', 'ge', NULL, NULL),
(74, 'French Guiana', 'gf', NULL, NULL),
(76, 'Ghana', 'gh', NULL, NULL),
(77, 'Gibraltar', 'gi', NULL, NULL),
(78, 'Greenland', 'gl', NULL, NULL),
(79, 'Gambia', 'gm', NULL, NULL),
(80, 'Guinea', 'gn', NULL, NULL),
(81, 'Guadeloupe', 'gp', NULL, NULL),
(82, 'Equatorial Guinea', 'gq', NULL, NULL),
(83, 'Greece', 'gr', NULL, NULL),
(84, 'S. Georgia and S. Sandwich Islands', 'gs', NULL, NULL),
(85, 'Guatemala', 'gt', NULL, NULL),
(86, 'Guinea-Bissau', 'gw', NULL, NULL),
(87, 'Guyana', 'gy', NULL, NULL),
(88, 'Hong Kong', 'hk', NULL, NULL),
(89, 'Honduras', 'hn', NULL, NULL),
(90, 'Croatia (Hrvatska)', 'hr', NULL, NULL),
(91, 'Haiti', 'ht', NULL, NULL),
(92, 'Hungary', 'hu', NULL, NULL),
(93, 'Indonesia', 'id', NULL, NULL),
(94, 'Ireland', 'ie', NULL, NULL),
(95, 'Israel', 'il', NULL, NULL),
(96, 'India', 'in', NULL, NULL),
(97, 'Iraq', 'iq', NULL, NULL),
(98, 'Iran', 'ir', NULL, NULL),
(99, 'Iceland', 'is', NULL, NULL),
(100, 'Italy', 'it', NULL, NULL),
(101, 'Jamaica', 'jm', NULL, NULL),
(102, 'Jordan', 'jo', NULL, NULL),
(103, 'Japan', 'jp', NULL, NULL),
(104, 'Kenya', 'ke', NULL, NULL),
(105, 'Kyrgyzstan', 'kg', NULL, NULL),
(106, 'Cambodia', 'kh', NULL, NULL),
(107, 'Kiribati', 'ki', NULL, NULL),
(108, 'Comoros', 'km', NULL, NULL),
(109, 'Saint Kitts and Nevis', 'kn', NULL, NULL),
(110, 'Korea (North)', 'kp', NULL, NULL),
(111, 'Korea (South)', 'kr', NULL, NULL),
(112, 'Kuwait', 'kw', NULL, NULL),
(113, 'Cayman Islands', 'ky', NULL, NULL),
(114, 'Kazakhstan', 'kz', NULL, NULL),
(115, 'Laos', 'la', NULL, NULL),
(116, 'Lebanon', 'lb', NULL, NULL),
(117, 'Saint Lucia', 'lc', NULL, NULL),
(118, 'Liechtenstein', 'li', NULL, NULL),
(119, 'Sri Lanka', 'lk', NULL, NULL),
(120, 'Liberia', 'lr', NULL, NULL),
(121, 'Lesotho', 'ls', NULL, NULL),
(122, 'Lithuania', 'lt', NULL, NULL),
(123, 'Luxembourg', 'lu', NULL, NULL),
(124, 'Latvia', 'lv', NULL, NULL),
(125, 'Libya', 'ly', NULL, NULL),
(126, 'Morocco', 'ma', NULL, NULL),
(127, 'Monaco', 'mc', NULL, NULL),
(128, 'Moldova', 'md', NULL, NULL),
(129, 'Madagascar', 'mg', NULL, NULL),
(130, 'Marshall Islands', 'mh', NULL, NULL),
(131, 'Macedonia', 'mk', NULL, NULL),
(132, 'Mali', 'ml', NULL, NULL),
(133, 'Myanmar', 'mm', NULL, NULL),
(134, 'Mongolia', 'mn', NULL, NULL),
(135, 'Macao', 'mo', NULL, NULL),
(136, 'Northern Mariana Islands', 'mp', NULL, NULL),
(137, 'Martinique', 'mq', NULL, NULL),
(138, 'Mauritania', 'mr', NULL, NULL),
(139, 'Montserrat', 'ms', NULL, NULL),
(140, 'Malta', 'mt', NULL, NULL),
(141, 'Mauritius', 'mu', NULL, NULL),
(142, 'Maldives', 'mv', NULL, NULL),
(143, 'Malawi', 'mw', NULL, NULL),
(144, 'Mexico', 'mx', NULL, NULL),
(145, 'Malaysia', 'my', NULL, NULL),
(146, 'Mozambique', 'mz', NULL, NULL),
(147, 'Namibia', 'na', NULL, NULL),
(148, 'New Caledonia', 'nc', NULL, NULL),
(149, 'Niger', 'ne', NULL, NULL),
(150, 'Norfolk Island', 'nf', NULL, NULL),
(151, 'Nigeria', 'ng', NULL, NULL),
(152, 'Nicaragua', 'ni', NULL, NULL),
(153, 'Netherlands', 'nl', NULL, NULL),
(154, 'Norway', 'no', NULL, NULL),
(155, 'Nepal', 'np', NULL, NULL),
(156, 'Nauru', 'nr', NULL, NULL),
(157, 'Niue', 'nu', NULL, NULL),
(158, 'New Zealand (Aotearoa)', 'nz', NULL, NULL),
(159, 'Oman', 'om', NULL, NULL),
(160, 'Panama', 'pa', NULL, NULL),
(161, 'Peru', 'pe', NULL, NULL),
(162, 'French Polynesia', 'pf', NULL, NULL),
(163, 'Papua New Guinea', 'pg', NULL, NULL),
(164, 'Philippines', 'ph', NULL, NULL),
(165, 'Pakistan', 'pk', NULL, NULL),
(166, 'Poland', 'pl', NULL, NULL),
(167, 'Saint Pierre and Miquelon', 'pm', NULL, NULL),
(168, 'Pitcairn', 'pn', NULL, NULL),
(169, 'Palestinian Territory', 'ps', NULL, NULL),
(170, 'Portugal', 'pt', NULL, NULL),
(171, 'Palau', 'pw', NULL, NULL),
(172, 'Paraguay', 'py', NULL, NULL),
(173, 'Qatar', 'qa', NULL, NULL),
(174, 'Reunion', 're', NULL, NULL),
(175, 'Romania', 'ro', NULL, NULL),
(176, 'Russian Federation', 'ru', NULL, NULL),
(177, 'Rwanda', 'rw', NULL, NULL),
(178, 'Saudi Arabia', 'sa', NULL, NULL),
(179, 'Solomon Islands', 'sb', NULL, NULL),
(180, 'Seychelles', 'sc', NULL, NULL),
(181, 'Sudan', 'sd', NULL, NULL),
(182, 'Sweden', 'se', NULL, NULL),
(183, 'Singapore', 'sg', NULL, NULL),
(184, 'Saint Helena', 'sh', NULL, NULL),
(185, 'Slovenia', 'si', NULL, NULL),
(186, 'Svalbard and Jan Mayen', 'sj', NULL, NULL),
(187, 'Slovakia', 'sk', NULL, NULL),
(188, 'Sierra Leone', 'sl', NULL, NULL),
(189, 'San Marino', 'sm', NULL, NULL),
(190, 'Senegal', 'sn', NULL, NULL),
(191, 'Somalia', 'so', NULL, NULL),
(192, 'Suriname', 'sr', NULL, NULL),
(193, 'Sao Tome and Principe', 'st', NULL, NULL),
(194, 'El Salvador', 'sv', NULL, NULL),
(195, 'Syria', 'sy', NULL, NULL),
(196, 'Swaziland', 'sz', NULL, NULL),
(197, 'Turks and Caicos Islands', 'tc', NULL, NULL),
(198, 'Chad', 'td', NULL, NULL),
(199, 'French Southern Territories', 'tf', NULL, NULL),
(200, 'Togo', 'tg', NULL, NULL),
(201, 'Thailand', 'th', NULL, NULL),
(202, 'Tajikistan', 'tj', NULL, NULL),
(203, 'Tokelau', 'tk', NULL, NULL),
(204, 'Turkmenistan', 'tm', NULL, NULL),
(205, 'Tunisia', 'tn', NULL, NULL),
(206, 'Tonga', 'to', NULL, NULL),
(207, 'Turkey', 'tr', NULL, NULL),
(208, 'Trinidad and Tobago', 'tt', NULL, NULL),
(209, 'Tuvalu', 'tv', NULL, NULL),
(210, 'Taiwan', 'tw', NULL, NULL),
(211, 'Tanzania', 'tz', NULL, NULL),
(212, 'Ukraine', 'ua', NULL, NULL),
(213, 'Uganda', 'ug', NULL, NULL),
(214, 'Uruguay', 'uy', NULL, NULL),
(215, 'Uzbekistan', 'uz', NULL, NULL),
(216, 'Saint Vincent and the Grenadines', 'vc', NULL, NULL),
(217, 'Venezuela', 've', NULL, NULL),
(218, 'Virgin Islands (British)', 'vg', NULL, NULL),
(219, 'Virgin Islands (U.S.)', 'vi', NULL, NULL),
(220, 'Viet Nam', 'vn', NULL, NULL),
(221, 'Vanuatu', 'vu', NULL, NULL),
(222, 'Wallis and Futuna', 'wf', NULL, NULL),
(223, 'Samoa', 'ws', NULL, NULL),
(224, 'Yemen', 'ye', NULL, NULL),
(225, 'Mayotte', 'yt', NULL, NULL),
(226, 'South Africa', 'za', NULL, NULL),
(227, 'Zambia', 'zm', NULL, NULL),
(228, 'Zaire (former)', 'zr', NULL, NULL),
(229, 'Zimbabwe', 'zw', NULL, NULL),
(230, 'United States of America', 'us', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `discount` double NOT NULL,
  `min_amount` double NOT NULL,
  `valid_until` date NOT NULL,
  `is_voucher` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `format` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `format`, `exchange_rate`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Euro', 'EUR', '€', '1.0,00 €', '85.09', 1, '2018-10-06 13:13:56', '2018-10-06 13:13:56'),
(2, 'US Dollar', 'USD', '$', '$1,0.00', '1', 1, '2018-10-06 13:15:02', '2018-10-06 13:15:02'),
(3, 'Indian Rupee', 'INR', '₹', '1,0.00₹', '73.77', 1, '2018-10-06 13:24:09', '2018-10-06 13:24:09'),
(4, 'Australian Dollar', 'AUD', '$', '$1,0.00', '1.42', 1, '2018-10-06 13:35:49', '2018-10-06 13:35:49'),
(5, 'Pound Sterling', 'GBP', '£', '£1,0.00', '0.76', 1, '2018-10-06 13:40:55', '2018-10-06 13:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imageable_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `url`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`) VALUES
(5, 'mahendra-5bc38292dd6cd.jpg', '5', 'App\\Models\\Products', '2018-10-14 12:23:22', '2018-10-14 12:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_13_132105_create_products_table', 1),
(4, '2018_09_13_132128_create_user_address_table', 1),
(5, '2018_09_13_132144_create_cart_table', 1),
(6, '2018_09_13_132406_create_categories_table', 1),
(7, '2018_09_13_132858_create_coupons_table', 1),
(8, '2018_09_13_133030_create_bookings_table', 1),
(9, '2018_09_13_133600_create_images_table', 1),
(10, '2018_09_13_134537_create_reviews_table', 1),
(11, '2018_09_13_135519_create_wishlist_table', 1),
(12, '2018_09_13_141026_create_visitor_table', 1),
(13, '2018_09_16_074911_create_countries_table', 1),
(16, '2018_09_19_192933_add_admin_column_in_users_table', 2),
(17, '2018_10_02_184920_remove_in_stock_column_in_product_table', 3),
(24, '2018_10_02_185506_create_colors_table', 4),
(25, '2018_10_02_185518_create_sizes_table', 4),
(26, '2018_10_02_185750_add_is_voucher_column_in_coupons_table', 4),
(27, '2018_10_03_170746_create_products_colors_table', 5),
(28, '2018_10_03_170813_create_products_sizes_table', 5),
(29, '2013_11_26_161501_create_currency_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `category_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_unique_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `original_price` double NOT NULL,
  `discount_price` double DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `uuid`, `category_uuid`, `name`, `product_unique_id`, `description`, `original_price`, `discount_price`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(5, '1f240f06-2e4a-4f69-95e9-42cde2f9431b', '9fa15667-c0f5-11e8-8b08-34e6d7235aff', 'mahendra', 'mah1861026898', 'fvrrt', 34, 4343, 1, NULL, '2018-10-14 12:23:22', '2018-10-14 12:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `products_colors`
--

CREATE TABLE `products_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_colors`
--

INSERT INTO `products_colors` (`id`, `product_id`, `color_id`, `created_at`, `updated_at`) VALUES
(6, 5, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_sizes`
--

CREATE TABLE `products_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_sizes`
--

INSERT INTO `products_sizes` (`id`, `product_id`, `size_id`, `created_at`, `updated_at`) VALUES
(6, 5, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `short_description`, `created_at`, `updated_at`) VALUES
(3, 'lg', 'larg', '2018-10-03 13:07:06', '2018-10-03 13:07:06'),
(4, 'sm', 'small', '2018-10-03 13:07:14', '2018-10-03 13:07:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `isAdmin` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `mobile`, `profile`, `country_id`, `password`, `is_active`, `isAdmin`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'b6d93a9d-bc43-11e8-9e10-34e6d7235aff', 'fafasd', 'mahendra22@gmail.com', 'q423423432432', 'ddwde', '23', '$2y$10$pHNI7EhUyhVuYDgfAkjqZ.L4PHTXoj5IpU2Q81NfJT6mboc2IFtRW', 1, 1, NULL, 'iKyGOWJBi4X17jL9M2RKYxjNxbSYwSiMUxBqjXljiB7UFkwwh4o9rahuVldK', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `address_one` text COLLATE utf8_unicode_ci NOT NULL,
  `address_two` text COLLATE utf8_unicode_ci,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `pin_code` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(10) UNSIGNED NOT NULL,
  `random_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `platform` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `user_uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currencies_code_index` (`code`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_colors`
--
ALTER TABLE `products_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_sizes`
--
ALTER TABLE `products_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products_colors`
--
ALTER TABLE `products_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products_sizes`
--
ALTER TABLE `products_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
