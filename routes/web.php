<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login routes
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Web
Route::group(['prefix' => '/', 'middleware' => 'web', 'namespace' => 'Web'], function () {

    Route::get('/', 'HomeController')->name('home');
    Route::post('/change-currency', 'HomeController@changeCurrency')->name('change-currency');

    // pages
    Route::get('/shop', 'ShopController')->name('shop');
    Route::post('/shop/filter', 'ShopController@filter')->name('shop.filter');

    Route::post('/cart/add', 'CartController@add')->name('cart.add');
    Route::get('/cart/remove/{uuid}', 'CartController@remove')->name('cart.remove');
    Route::get('/cart/qty/{uuid}/{qty}', 'CartController@qtyModify')->name('cart.qty');
//    Route::get('/cart/generate', 'CartController@generate')->name('cart.generate');

    Route::get('/cart', 'CartController')->name('cart');
    Route::get('/invoice-download/{uuid}','CartController@downloadInvoice')->name('invoice-download');

    Route::get('/product/{uuid}', 'ProductController')->name('product');
    Route::get('/faq', 'PageController@faq')->name('faq');
    Route::get('/about-us', 'PageController@aboutUs')->name('about-us');
    Route::get('/contact', 'PageController@contact')->name('contact');
    Route::post('/contact', 'PageController@submitContact')->name('contact');
    Route::get('/search', 'SearchController')->name('search');
    Route::get('/favourite', 'FavouriteController')->name('favourite');
    Route::get('/checkout', 'CheckoutController')->name('checkout');
    Route::post('/checkout/guest','CheckoutController@guest')->name('checkout.guest');

    # Call Route
        Route::get('payment', ['as' => 'payment', 'uses' => 'PaymentController@payment']);

# Status Route
    Route::get('payment/status', ['as' => 'payment.status', 'uses' => 'PaymentController@status']);

});
// return response image file
Route::get('/file/{response_type}/{storage}/{filename}', function ($responseType, $storage, $filename) {
    if (!Storage::disk($storage)->exists($filename)) {
        abort(404);
    }
    $file = Storage::disk($storage)->get($filename);
    $type = Storage::disk($storage)->mimeType($filename);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    if ($responseType == 'url') {
        return $response;
    } else {
        return response()->download(storage_path() . "\\app\\{$storage}\\{$filename}");
    }
})->name('file');


// return response image file
Route::get('/file/{response_type}/{storage}/{uuid}/{filename}', function ($responseType, $storage, $uuid, $filename) {
    if (!Storage::disk($storage)->exists($uuid . '/' . $filename)) {
        abort(404);
    }
    $file = Storage::disk($storage)->get($uuid . '/' . $filename);
    $type = Storage::disk($storage)->mimeType($uuid . '/' . $filename);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    if ($responseType == 'url') {
        return $response;
    } else {
        return response()->download(storage_path() . "\\app\\public\\{$storage}\\{$uuid}\\{$filename}");
    }
})->name('file.uuid');
// Admin
Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'admin'], 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', 'DashboardController')->name('dashboard');
    Route::get('/bookings', 'BookingsController')->name('bookings');
    // category section
    Route::get('/categories', 'CategoriesController')->name('categories');
    Route::post('/create-category', 'CategoriesController@create')->name('create-category');
    Route::post('/update-category', 'CategoriesController@update')->name('update-category');
    Route::get('/category-edit/{uuid}', 'CategoriesController@edit')->name('category.edit');
    Route::get('/category-trash/{uuid}', 'CategoriesController@trash')->name('category.trash');
    Route::get('/category-restore/{uuid}', 'CategoriesController@restore')->name('category.restore');
    Route::get('/category-delete/{uuid}', 'CategoriesController@deletePermanent')->name('category.delete');

    // products section
    Route::get('/products', 'ProductsController')->name('products');
    Route::post('/create-product', 'ProductsController@create')->name('create-product');
    Route::post('/update-product', 'ProductsController@update')->name('update-product');
    Route::get('/product-edit/{uuid}', 'ProductsController@edit')->name('product.edit');
    Route::get('/product-trash/{uuid}', 'ProductsController@trash')->name('product.trash');
    Route::get('/product-restore/{uuid}', 'ProductsController@restore')->name('product.restore');
    Route::get('/product-delete/{uuid}', 'ProductsController@deletePermanent')->name('product.delete');
    Route::post('product/images-delete', ['uses' => 'ProductsController@deleteImages'])->name('product-images-delete');


    // colors section
    Route::get('/product-colors', 'ColorsController')->name('colors');
    Route::post('/create-product-colors', 'ColorsController@create')->name('create-color');
    Route::get('/color-delete/{id}', 'ColorsController@deletePermanent')->name('color.delete');


    // sizes section
    Route::get('/product-sizes', 'SizesController')->name('sizes');
    Route::post('/create-product-sizes', 'SizesController@create')->name('create-size');
    Route::get('/size-delete/{id}', 'SizesController@deletePermanent')->name('size.delete');


    Route::get('/guest-users', 'GuestUserController')->name('guest-users');
    Route::get('/reviews', 'ReviewsController')->name('reviews');
    Route::get('/transactions', 'TransactionsController')->name('transactions');
    Route::get('/user-carts', 'CartController')->name('user-carts');
    Route::post('/user-carts/items', 'CartController@items')->name('user-carts.items');
});



