<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'comment',
        'first_name',
        'last_name',
        'session_id',
        'email',
        'number',
        'country',
        'city',
        'code',
        'address',
        'cart_id'
    ];

    public function getNameAttribute()
    {
        return $this->first_name .' '. $this->last_name;
    }

    public function countryRelation()
    {
        return $this->belongsTo('App\Models\Country','country','code');
    }

    public function cart()
    {
        return $this->belongsTo('App\Models\Cart','cart_id');
    }

    public function booking()
    {
        return $this->hasOne('App\Models\Booking','guest_id');
    }
}
