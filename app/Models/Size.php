<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = ['name', 'short_description'];

    public function product()
    {
        return $this->belongsToMany('Models/Products');
    }
}
