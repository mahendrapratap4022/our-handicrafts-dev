<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayuPayment extends Model
{
    protected $casts = [
        'data' => 'array'
    ];

    protected $fillable = [
        'currency_type'
    ];

    public function cart()
    {
        $this->hasOne('App\Models\Cart','txnid');
    }
}
