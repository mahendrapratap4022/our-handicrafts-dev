<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    protected $fillable = ['uuid', 'name', 'description', 'start_form', 'end_to', 'image', 'parent_uuid', 'status'];
    use SoftDeletes;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeMainCategories($query)
    {
        return $query->whereNull('parent_uuid');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Products','category_uuid','uuid');
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }
}
