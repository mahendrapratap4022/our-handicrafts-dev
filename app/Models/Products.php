<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    protected $fillable = ['uuid', 'category_uuid', 'name', 'product_unique_id', 'description', 'original_price', 'discount_price', 'status'];
    use SoftDeletes;

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1)->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('categories')
                ->where('status', 1)
                ->whereRaw('categories.uuid = products.category_uuid');
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Categories', 'category_uuid', 'uuid');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Images', 'imageable');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Models\Color', 'products_colors', 'product_id', 'color_id');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Models\size', 'products_sizes', 'product_id', 'size_id');
    }

    public function getSizeListAttribute()
    {
        return $this->sizes->pluck('name','name');
    }

    public function getColorListAttribute()
    {
        return $this->colors->pluck('name', 'id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }
}
