<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class CartItem extends Model
{
    //
    protected $fillable = [
        'product_uuid',
        'qty',
        'cart_id',
        'size',
        'color'
    ];

    protected $appends = [
        'total_price'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }

    public function getRouteKey()
    {
        return $this->uuid;
    }

    public function cart()
    {
        return $this->belongsTo('App/Models/Cart');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Products','uuid','product_uuid');
    }


    public function getTotalPriceAttribute() {
        return $this->product->discount_price * $this->qty;
    }
}
