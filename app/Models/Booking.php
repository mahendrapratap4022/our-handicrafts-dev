<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
      'order_id',
      'guest_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order_id = strtoupper(str_random(16));
        });
    }

    public function guest()
    {
        return $this->belongsTo('App\Models\Guest','guest_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\PayuPayment','order_id','txnid');
    }

}
