<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Webpatser\Uuid\Uuid;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = [
        'session_id',
        'user_uuid',
        'visitor_uuid',
        'cart_random_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
            $model->cart_random_id = strtoupper(str_random(8));
        });
    }

    public function items()
    {
        return $this->hasMany('App\Models\CartItem');
    }

    public function guest()
    {
        return $this->hasOne('App\Models\Guest','cart_id');
    }

    public function getTotalAmountAttribute()
    {
        $total_amount = 0;
        foreach ($this->items as $item) {
            $total_amount += $item->product->discount_price * $item->qty;
        }
        return $total_amount;
    }

    public function scopeGuestCart($q)
    {
        return $q->where('session_id', Session::getId());
    }
}
