<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 13-12-2018
 * Time: 00:30
 */

namespace App\Http\Helpers;


use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    private $mail;

    public function send($to, $data)
    {

        //PHPMailer Object
        $this->mail = new PHPMailer;

        //From email address and name
        $this->mail->From = "info@ourhandicraft.com";
        $this->mail->FromName = "Test Dev";

        //To address and name
        $this->mail->addAddress($to);

        //Address to which recipient will reply
        $this->mail->addReplyTo("info@ourhandicraft.com", "Reply");

        //Send HTML or Plain Text email
        $this->mail->isHTML(true);
        $this->mail->SMTPAuth = false;
        $this->mail->Subject = "Invoice from ourhandicraft.com";
        $view = view('emails.payment.test', compact('data'));
        $view->render();
        $this->mail->Body = $view;

        if (!$this->mail->send()) {
            echo "<script>alert('Mailer Error: ".$this->mail->ErrorInfo."')</script>";
        } else {
            echo "<script>alert('success')</script>";
        }
    }
}