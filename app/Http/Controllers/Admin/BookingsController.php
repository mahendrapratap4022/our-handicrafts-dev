<?php

namespace App\Http\Controllers\Admin;

use App\Models\Booking;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class BookingsController extends Controller
{
    public function __invoke()
    {
        $bookings = Booking::has('payment')->with('payment','guest')->paginate(10);

        return view('admin.bookings',compact('bookings'));
    }
}
