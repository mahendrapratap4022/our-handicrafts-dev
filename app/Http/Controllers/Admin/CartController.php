<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function __invoke()
    {
        return view('admin.cart.user-carts');
    }

    public function items(Request $request)
    {
        $request->validate([
            'cart_id' => 'required|exists:cart,cart_random_id'
        ]);

        $cart = Cart::with('items')->where('cart_random_id',$request->cart_id)->first();
        return view('admin.cart.list-items',compact('cart'));
    }
}
