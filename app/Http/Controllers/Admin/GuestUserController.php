<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuestUserController extends Controller
{
    public function __invoke()
    {
        return view('admin.guest-users');
    }
}
