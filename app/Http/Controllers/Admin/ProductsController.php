<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Color;
use App\Models\Products;
use App\Models\Size;
use App\Models\Images;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;


class ProductsController extends Controller
{
    public function __invoke()
    {
        $categories = Categories::active()->pluck('name', 'uuid');
        $colors = Color::all()->pluck('name', 'id');
        $sizes = Size::all()->pluck('name', 'id');
        $allproducts = Products::all();
        $trashedproducts = Products::onlyTrashed()->get();
        return view('admin.products.index', compact('categories', 'colors', 'sizes','allproducts','trashedproducts'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'images' => 'array|required',
            'status' => 'required|in:0,1',
            'category_uuid' => 'required',
            'original_price' => 'required|numeric',
            'discount_price' => 'required|numeric',
            'colors' => 'required|array',
            'sizes' => 'required|array',
            'description' => 'required',
        ]);
        $request['product_unique_id'] = substr($request['name'], 0, 3) . mt_rand();
        $product = $this->updateOrCreate($request);

        $product->sizes()->sync($request->sizes);
        $product->colors()->sync($request->colors);
        // Upload Images
        if ($request->hasFile('images')) {
            $this->uploadImages($request, $product);
        }
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Product successfully created!',
            'messageText' => 'This Product is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required|in:0,1',
            'category_uuid' => 'required',
            'original_price' => 'required|numeric',
            'discount_price' => 'required|numeric',
            'colors' => 'required|array',
            'sizes' => 'required|array',
            'description' => 'required',
        ]);
        $request['product_unique_id'] = substr($request['name'], 0, 3) . mt_rand();
        $product = $this->updateOrCreate($request);

        $product->sizes()->sync($request->sizes);
        $product->colors()->sync($request->colors);
        // Upload Images
        if ($request->hasFile('images')) {
            $this->uploadImages($request, $product);
        }
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Product successfully updated!',
            'messageText' => 'This Product is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }

    protected function updateOrCreate(Request $request)
    {
        return Products::updateOrCreate(
            [
                'uuid' => $request['uuid']
            ],
            [
                'name' => $request['name'],
                'product_unique_id' => $request['product_unique_id'],
                'description' => $request['description'],
                'status' => $request['status'],
                'category_uuid' => $request['category_uuid'],
                'original_price' => $request['original_price'],
                'discount_price' => $request['discount_price'],
            ]
        );
    }

    public function uploadImages(Request $request, $product)
    {
        foreach ($request->file('images') as $image) {
            $filename = str_replace('/', '-', $request->name) . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            $zoomImage = Image::make($image)->fit(1200)->encode('jpg');

            Storage::disk('zoom')->put($product->uuid . "/" . $filename, $zoomImage);

            $image = Image::make($image)->fit(300)->encode('jpg');

            Storage::disk('products')->put($product->uuid . "/" . $filename, $image);

            $image = new Images();
            $image->url = $filename;
            $product->images()->save($image);
        }
    }

    public function edit($uuid)
    {
        $product = Products::with('images')->where('uuid', $uuid)->first();
        $categories = Categories::active()->pluck('name', 'uuid');
        $colors = Color::all()->pluck('name', 'id');
        $sizes = Size::all()->pluck('name', 'id');
        if ($product->count() > 0) {
            return view('admin.products.edit', compact('categories', 'colors', 'sizes','product'));
        } else {
            return abort(404);
        }
    }

    public function trash($uuid)
    {
        $product = Products::where('uuid', $uuid)->first();
        if ($product) {
            $product->update(['status' => 0]);
            $product->delete();
            // return
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Product successfully Deleted!',
                'messageText' => 'This Product  is no more available for the services',
                'code' => '200'
            ]);
        } else {
            return abort(404);
        }
    }

    public function restore($uuid)
    {
        $product = Products::withTrashed()->where('uuid', $uuid)->first();
        if ($product) {
            $product->restore();
            // return
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Product successfully Restored!',
                'messageText' => 'This Product is Now available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }

    public function deletePermanent($uuid)
    {
        $product = Products::withTrashed()->where('uuid', $uuid)->first();
        if ($product) {
            Storage::disk('products')->deleteDirectory($product->uuid);
            $product->sizes()->detach();
            $product->colors()->detach();
            $product->images()->delete();
            $product->forceDelete();
            // return
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Product successfully Permanent Deleted!',
                'messageText' => 'This Product is no more available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }

    public function deleteImages(Request $request)
    {
        if (!$request->image_id) {
            return back()->with([
                'type' => 'error',
                'messageTitle' => 'No Images Selected!',
                'messageText' => 'Please select image and then click on delete button.',
                'code' => '204'
            ]);
        }
        $product = Products::where('uuid', $request->uuid)->first();
        if ($product->count() > 0) {
            foreach ($request->image_id as $id) {
                $name = $product->images()->where('id', $id)->first()->url;
                Storage::disk('products')->delete("/$product->uuid/".$name);
                $product->images()->where('id', $id)->delete();
            }
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Images Deleted!',
                'messageText' => 'Images deleted successfully!',
                'code' => '201'
            ]);
        }
    }
}
