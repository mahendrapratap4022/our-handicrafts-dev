<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CategoriesController extends Controller
{
    public function __invoke()
    {
        $allcategories = Categories::all();
        $trashedcategories = Categories::onlyTrashed()->get();
        $categories = Categories::mainCategories()->pluck('name', 'uuid')->toArray();
        $selectCategory = array_merge([NULL  => 'Select parent category if this is child'], $categories);
        return view('admin.categories.index', compact('selectCategory', 'allcategories', 'trashedcategories'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => '',
            'start_form' => 'required|numeric',
            'end_to' => 'numeric|nullable',
            'image' => 'required|image',
            'status' => 'required|in:0,1',
            'category' => '',
        ]);

        // Upload Images
        $request['file'] = NULL;
        if ($request->hasFile('image')) {
            $request['file'] = $request->name . '-' . uniqid() . '.' . $request->image->getClientOriginalExtension();
            Storage::disk('categories')->put($request['file'], File::get($request->image));
        }
        $data = [
            'name' => $request->name,
            'image' => $request['file'],
            'start_form' => $request->start_form,
            'end_to' => $request->end_to,
            'status' => $request->status,
            'parent_uuid' => $request->category,
            'description' => $request->description,
        ];
        Categories::create($data);
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Category successfully created!',
            'messageText' => 'This Category is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => '',
            'start_form' => 'required|numeric',
            'end_to' => 'numeric|nullable',
            'image' => 'nullable|image',
            'status' => 'required|in:0,1',
            'category' => '',
        ]);

        // Upload Images
        $data = [
            'name' => $request->name,
            'start_form' => $request->start_form,
            'end_to' => $request->end_to,
            'status' => $request->status,
            'parent_uuid' => $request->category,
            'description' => $request->description,
        ];

        if ($request->hasFile('image')) {
            $request['file'] = $request->name . '-' . uniqid() . '.' . $request->image->getClientOriginalExtension();
            Storage::disk('categories')->put($request['file'], File::get($request->image));
            $data['image'] = $request['file'];
        }

        Categories::where('uuid',$request->uuid)->update($data);
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Category successfully created!',
            'messageText' => 'This Category is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }

    public function edit($uuid)
    {
        $category = Categories::where('uuid', $uuid)->first();
        $categories = Categories::mainCategories()->pluck('name', 'uuid')->toArray();
        $selectCategory = array_merge([NULL => 'Select parent category if this is child'], $categories);
        if ($category->count() > 0) {
            return view('admin.categories.edit', compact('category', 'selectCategory'));
        } else {
            return abort(404);
        }
    }

    public function trash($uuid)
    {
        $category = Categories::where('uuid', $uuid)->first();
        if ($category) {
            $category->update(['status' => 0]);
            $category->delete();
            // return
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Categories successfully Deleted!',
                'messageText' => 'This Categories is no more available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }

    public function restore($uuid)
    {
        $category = Categories::withTrashed()->where('uuid', $uuid)->first();
        if ($category) {
            $category->restore();
            // return
            return back()->with([
                'type' => 'success',
                'messageTitle' => 'Categories successfully Restored!',
                'messageText' => 'This Categories is Now available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }

    public function deletePermanent($uuid)
    {
        $category = Categories::withTrashed()->where('uuid', $uuid)->first();
        if ($category) {
            foreach ($category->products as $product){
                Storage::disk('products')->deleteDirectory($product->uuid);
                $product->forceDelete();
            }
            Storage::disk('categories')->delete($category->image);
            $category->forceDelete();
            // return
            return Redirect::route('admin.categories')->with([
                'type' => 'success',
                'messageTitle' => 'Categories successfully Permanent Deleted!',
                'messageText' => 'This Categories is no more available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }
}
