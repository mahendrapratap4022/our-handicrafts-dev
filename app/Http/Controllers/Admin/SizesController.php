<?php

namespace App\Http\Controllers\Admin;

use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SizesController extends Controller
{
    public function __invoke()
    {
        $sizes = Size::all();
        return view('admin.sizes.index', compact('sizes'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'short_description' => 'required',
        ]);
        Size::create($request->all());
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Category successfully created!',
            'messageText' => 'This Category is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }

    public function deletePermanent($id)
    {
        $size = Size::find($id)->first();
        if ($size) {
            $size->delete();
            // return
            return Redirect::route('admin.sizes')->with([
                'type' => 'success',
                'messageTitle' => 'Size successfully Permanent Deleted!',
                'messageText' => 'This Size is no more available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }
}
