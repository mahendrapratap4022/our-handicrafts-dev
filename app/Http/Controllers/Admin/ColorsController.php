<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ColorsController extends Controller
{
    public function __invoke()
    {
        $colors = Color::all();
        return view('admin.colors.index', compact('colors'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'hexa' => 'required',
        ]);
        Color::create($request->all());
        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Category successfully created!',
            'messageText' => 'This Category is now available in the products section if its activated.',
            'code' => '201'
        ]);
    }

    public function deletePermanent($id)
    {
        $color = Color::find($id)->first();
        if ($color) {
            $color->delete();
            // return
            return Redirect::route('admin.colors')->with([
                'type' => 'success',
                'messageTitle' => 'color successfully Permanent Deleted!',
                'messageText' => 'This color is no more available for the services',
                'code' => '201'
            ]);
        } else {
            return abort(404);
        }
    }
}
