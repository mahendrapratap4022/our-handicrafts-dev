<?php

namespace App\Http\Controllers\Web;

use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function __invoke($uuid)
    {
        // TODO: Implement __invoke() method.
        $product = Products::with('sizes','colors')->where('uuid',$uuid)->first();
        return view('web.product',compact('product'));
    }
}
