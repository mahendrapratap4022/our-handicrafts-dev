<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Models\Cart;
use App\Models\CartItem;
use App\Services\Calculation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\IpUtils;
use PDF;
class CartController extends Controller
{
    public function __invoke(Calculation $calculation)
    {
        return view('web.cart', compact('calculation'));
    }

    public function add(Request $request)
    {
        $request->validate([
            'qty' => 'required',
            'size' => 'required',
            'color' => 'required',
            'uuid' => 'required'
        ]);

        $cart = Cart::updateOrCreate(['session_id' => Session::getId()]);

        $cart_item = CartItem::updateOrCreate(
                ['product_uuid' => $request['uuid'], 'color' => $request['color'], 'size' => $request['size'], 'cart_id' => $cart->id],
                ['qty' => $request['qty']]
            );

        return response()->json(['id' => $cart_item->id],200);
    }

    public function remove($uuid)
    {
        CartItem::where('uuid',$uuid)->forceDelete();
        return back();
    }

    public function qtyModify($uuid,$qty)
    {
        CartItem::where('uuid',$uuid)->update([
            'qty' => $qty
        ]);

        return response()->json('success',200);
    }


    public function downloadInvoice($order_id)
    {
        $currentBooking = Booking::where('order_id',$order_id)->first();
        if(!$currentBooking){
            abort(404);
        }

        $pdf=PDF::loadView('admin.invoice', compact('currentBooking'));
        return $pdf->download('invoice.pdf');
    }
}
