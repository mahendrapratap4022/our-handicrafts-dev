<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Models\Cart;
use App\models\Country;
use App\Models\Guest;
use App\Services\Calculation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $countries = Country::pluck('name','code');
        view()->share(compact('countries'));
    }

    public function __invoke(Calculation $calculation)
    {
        if ($calculation->total <= 0){
            return back()->with([
                'type' => 'error',
                'messageTitle' => 'Cart is empty!',
                'messageText' => 'You cant proceed to checkout with empty cart. please put some products in your cart and try again.',
                'code' => '400'
            ]);
        }

        return view('web.checkout',compact('calculation'));
    }

    public function guest(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'number' => 'required',
            'email' => 'required',
            'country' => 'required',
            'address' => 'required',
            'city' => 'required',
            'code' => 'required',
        ]);

        $cart = Cart::GuestCart()->first();

        $guest = Guest::updateOrCreate(
            ['cart_id' => $cart->id],[
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'number' => $request->number,
            'email' => $request->email,
            'country' => $request->country,
            'address' => $request->address,
            'city' => $request->city,
            'code' => $request->code,
            'comment' => $request->comment
        ]);

        $booking = Booking::updateOrCreate([
           'guest_id' => $guest->id
        ], [
            'order_id' => strtoupper(str_random(16))
        ]);

        return redirect()->route('payment');
    }
}
