<?php

namespace App\Http\Controllers\Web;

use App\Models\Categories;
use App\Models\Color;
use App\Models\Products;
use App\Models\Size;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{
    private $filter;

    public function __construct()
    {
        // Load your objects
        $categories = Categories::active()->get();
        $colors = Color::pluck('hexa');
        $sizes = Size::get();
        // Make it available to all views by sharing it
        view()->share(compact('colors', 'categories', 'sizes'));
    }

    public function __invoke()
    {
        $category = new \stdClass();
        $category->products = Products::with('images')->active()->get();

//        dd($category);
        return view('web.shop', compact('category'));
    }

    public function filter(Request $request)
    {
        $this->filter = $request->all();

        $products = Products::active();

        if ($request->filled('colors')) {
            $products->whereHas('colors', function ($color) {
                return $color->whereIn('hexa', $this->filter['colors']);
            });
        }
        if ($request->filled('size') && $request->size != 'all') {
            $products->whereHas('sizes', function ($color) {
                $color->where('name', $this->filter['size']);
            });

        }
        if ($request->filled('category')) {
            $products->where('category_uuid', $request->category);

        }

        if($request->filled('search')){
            $products->where('name','like','%'.$request->search.'%');
        }
        if ($request->filled('sort_by') && $request->sort_by == 0) {
            $products->orderBy('created_at', 'desc');
        } else {
            $products->orderBy('created_at', 'asc');
        }
        $products = $products->paginate(8);
        if(!$request->filled('view') || $request->view == 'grind') {
            $render = view('web.render.products.grind', compact('category', 'products'))->render();
        } else {
            $render = view('web.render.products.list', compact('category', 'products'))->render();
        }
        return response()->json(array('success' => true, 'html' => $render), 200);
    }

}
