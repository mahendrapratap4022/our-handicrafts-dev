<?php

namespace App\Http\Controllers\Web;

use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function __invoke()
    {
        $catergories = Categories::active()->get();

        return view('web.home',compact('catergories'));
    }

    public function changeCurrency(Request $request)
    {
        if (currency()->hasCurrency($request->code)) {
            session(['currency' => $request->code]);
        }
        return back();
    }
}
