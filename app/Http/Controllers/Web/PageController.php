<?php

namespace App\Http\Controllers\Web;

use App\Mail\contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function faq()
    {
        return view('web.faq');
    }

    public function aboutUs()
    {
        return view('web.about-us');
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function submitContact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Mail::send(new contact($request->all()));


        return back()->with([
            'type' => 'success',
            'messageTitle' => 'Message sent successfully!',
            'messageText' => 'We will get back to you later.',
            'code' => '201'
        ]);
    }
}
