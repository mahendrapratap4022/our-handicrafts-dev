<?php

namespace App\Http\Controllers\Web;

use App\Mail\PaymentSuccess;
use App\Models\Booking;
use App\Models\Cart;
use App\Services\Calculation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Tzsk\Payu\Facade\Payment;

class PaymentController extends Controller
{
    public function payment(Calculation $calculation)
    {

        try {
            $attributes = [
                'txnid' => $calculation->cart->guest->booking->order_id, # Transaction ID.
                'amount' => currency($calculation->total, 'USD', 'INR', '!'), # Amount to be charged.
                'productinfo' => $calculation->cart->items->pluck('uuid'),
                'firstname' => $calculation->cart->guest->first_name, # Payee Name.
                'email' => $calculation->cart->guest->email, # Payee Email Address.
                'phone' => $calculation->cart->guest->number, # Payee Phone Number.
            ];
        } catch (\Exception $e) {

            return view('web.warning');
        }

        return Payment::make($attributes, function ($then) {
            $then->redirectRoute('payment.status');
        });
    }

    public function status()
    {
        try {
            $payment = Payment::capture();
            // Get the payment status.
            $payment->save();
        } catch (\Exception $e) {
            return view('web.warning');
        }

        if ($payment->isCaptured()) {
            $booking = Booking::where('order_id',$payment->txnid)->first();
            if(!$booking){
                return view('web.failed', [
                    'error' => 'something technical error occured',
                    'payment' => $payment
                ]);
            }

            $url = route('invoice-download',$booking->order_id);
            Mail::to($booking->guest->email)->send(new PaymentSuccess($url));

            Session::regenerate(true);
            Session::save();

            return view('web.successfull',compact('url','payment'));
        };

        $data = json_decode($payment->data);

        return view('web.failed', [
            'error' => $data->error_Message,
            'payment' => $payment
        ]);
    }

}
