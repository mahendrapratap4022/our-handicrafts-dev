<?php

namespace App\Currency\Formatters;

use Torann\Currency\Contracts\FormatterInterface;
use App\Models\Currency;
class WithoutComa implements FormatterInterface
{
    /**
     * Config options.
     *
     * @var array
     */
    protected $config;

    /**
     * Create a new instance.
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function format($value, $code = null)
    {
        $currency = Currency::where('code',$code)->first();
        if(session('currency') == 'EUR'){
            $value = str_replace(',','.',$value);
        } else {
            $value = str_replace(',','',$value);
        }

        return $currency->symbol . $value;
    }
}