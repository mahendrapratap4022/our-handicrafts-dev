<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 18-11-2018
 * Time: 17:36
 */

namespace App\Services;


use App\Models\Cart;
use Illuminate\Support\Facades\Session;

class Calculation
{

    public $subtotal = 0;
    public $discount = 0;
    public $total = 0;
    public $cart;
    public $product_count = 0;

    public function __construct()
    {
        $cart = Cart::with('items', 'guest')->where('session_id', Session::getId())->first();
        $this->cart = $cart;
        if ($cart) {
            $this->product_count = $cart->items->count();
            foreach ($cart->items as $item) {
                $this->subtotal += $item->product->discount_price * $item->qty;
                $this->discount += 0;
                $this->total += $item->product->discount_price * $item->qty;
            };
        }
    }
}