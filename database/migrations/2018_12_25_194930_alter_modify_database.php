<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterModifyDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('guest');
        Schema::dropIfExists('user_address');
        Schema::dropIfExists('visitor');
        Schema::dropIfExists('wishlist');
        Schema::dropIfExists('reviews');
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('booking_details', 'status', 'amount');
            $table->integer('guest_id')->after('order_id');
        });
        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn('user_uuid');
            $table->dropColumn('visitor_uuid');
        });

        Schema::table('guests', function (Blueprint $table) {
            $table->dropColumn('session_id');
            $table->integer('cart_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
