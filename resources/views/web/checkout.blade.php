@extends('web.layouts.main')
@section('content')
    <div class="cart-table-area section-padding-100">
        <form action="{{ route('checkout.guest') }}" method="post">
{{ csrf_field() }}
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="mt-50 clearfix">

                        <div class="cart-title">
                            <h2>Checkout</h2>
                        </div>

                            <div class="row">
                                <div class="col-md-6 mb-3 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    {!! Form::text('first_name',old('first_name'),['class' => 'form-control','placeholder' => 'First Name']) !!}
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                           {{ $errors->first('first_name') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-md-6 mb-3" {{ $errors->has('last_name') ? ' has-error' : '' }}>
                                    {!! Form::text('last_name',old('last_name'),['class' => 'form-control','placeholder' => 'Last Name']) !!}
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                           {{ $errors->first('last_name') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-12 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    {!! Form::text('email',old('email'),['class' => 'form-control','placeholder' => 'email@example.com']) !!}
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                           {{ $errors->first('email') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-12 mb-3 {{ $errors->has('country') ? ' has-error' : '' }}">
                                    {!! Form::select('country',$countries,old('country'),['class' => 'w-100', 'id' => 'country2']) !!}
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                           {{ $errors->first('country') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-12 mb-3 {{ $errors->has('address') ? ' has-error' : '' }}">
                                    {!! Form::text('address',old('address'),['class' => 'form-control','placeholder' => 'Address']) !!}
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                           {{ $errors->first('address') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-12 mb-3 {{ $errors->has('code') ? ' has-error' : '' }}">
                                    {!! Form::number('code',old('code'),['class' => 'form-control','placeholder' => '313001']) !!}
                                    @if ($errors->has('code'))
                                        <span class="help-block">
                           {{ $errors->first('code') }}
                      </span>
                                    @endif
                                </div>

                                <div class="col-md-6 mb-3 {{ $errors->has('city') ? ' has-error' : '' }}">
                                    {!! Form::text('city',old('city'),['class' => 'form-control','placeholder' => 'City']) !!}
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                           {{ $errors->first('city') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-md-6 mb-3 {{ $errors->has('number') ? ' has-error' : '' }}">
                                    {!! Form::text('number',old('number'),['class' => 'form-control','placeholder' => '91XXXXXXXXX']) !!}
                                    @if ($errors->has('number'))
                                        <span class="help-block">
                           {{ $errors->first('number') }}
                      </span>
                                    @endif
                                </div>
                                <div class="col-12 mb-3 {{ $errors->has('comment') ? ' has-error' : '' }}">
                                    <textarea name="comment" class="form-control w-100" id="comment" cols="30" rows="10"
                                              placeholder="Leave a comment about your order">{{ old('comment') }}</textarea>
                                    @if ($errors->has('comment'))
                                        <span class="help-block">
                           {{ $errors->first('comment') }}
                      </span>
                                    @endif
                                </div>

                                {{--<div class="col-12">--}}
                                {{--<div class="custom-control custom-checkbox d-block mb-2">--}}
                                {{--<input type="checkbox" class="custom-control-input" id="customCheck2">--}}
                                {{--<label class="custom-control-label" for="customCheck2">Create an accout</label>--}}
                                {{--</div>--}}
                                {{--<div class="custom-control custom-checkbox d-block">--}}
                                {{--<input type="checkbox" class="custom-control-input" id="customCheck3">--}}
                                {{--<label class="custom-control-label" for="customCheck3">Ship to a different--}}
                                {{--address</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="cart-summary">
                        <h5>Cart Total</h5>
                        <ul class="summary-table">
                            <li><span>subtotal:</span>
                                <span>{!! currency( $calculation->subtotal , 'USD', session('currency')) !!}</span></li>
                            <li><span>delivery:</span> <span>Free</span></li>
                            <li><span>discount:</span>
                                <span>-{!! currency( $calculation->discount , 'USD', session('currency')) !!}</span>
                            </li>
                            <li><span>total:</span>
                                <span>{!! currency( $calculation->total , 'USD', session('currency')) !!}</span></li>
                        </ul>

                        <div class="cart-btn mt-100">
                            <button type="submit" class="btn amado-btn w-100">Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
@stop
@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('#country2').select2();
    </script>
@stop
