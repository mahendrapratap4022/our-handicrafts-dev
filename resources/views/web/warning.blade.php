<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="format-detection" content="telephone=no">

    <!-- Bootstrap core css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <!-- font_awesome css -->
    <!-- coustom css here -->
    <link href="{{ asset('css/core-style.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/colors/default.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/font-awesome.min.css') }}" type="text/css" rel="stylesheet"/>
</head>

<body>
<section id="wrapper" class="error-page" style="margin-top: 30vh">
    <div class="error-box">
        <div class="error-body text-center">
            <i class="fa fa-exclamation" style="background: #e0a800; font-size: 60px; border-radius: 50%;padding: 40px;color: white"></i>
            <h3 class="text-uppercase">Warning!</h3>
            <p class="text-muted m-t-30 m-b-30 text-uppercase">Your payment is undone. Please go back and try again.</p>
            <a href="{{url('/')}}" onclick="ga('send','pageview')" class="btn btn-warning btn-rounded waves-effect waves-light m-b-40">Back to home</a></div>
    </div>
</section>
<!-- jQuery -->
<script src="{{ asset('js/jquery/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>

</html>