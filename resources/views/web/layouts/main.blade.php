@include('web.partials.header')
<div id="loader"><img src="{{ asset('images/loader.gif') }}" alt=""></div>
@include('web.partials.search')
<!-- ##### Main Content Wrapper Start ##### -->
<div class="main-content-wrapper d-flex clearfix">
    @include('web.partials.nav')
    @yield('content')
</div>
@include('web.partials.footer')