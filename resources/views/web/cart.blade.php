@extends('web.layouts.main')
@section('content')
    <!-- Product Catagories Area Start -->
    <div class="cart-table-area section-padding-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="cart-title mt-50">
                        <h2>Shopping Cart({{ $calculation->cart->cart_random_id ?? 0 }})</h2>
                        {{---<a href="{{ route('cart.generate') }}"> Generate Cart</a>--}}
                    </div>

                    <div class="cart-table clearfix">
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Size</th>
                                <th>Color</th>
                                <th>Quantity</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($calculation->cart->items))
                                @forelse($calculation->cart->items as $key => $item)
                                    @if($item->qty > 0)
                                        <tr>
                                            <td class="cart_product_img">
                                                <a href="#"><img
                                                            src="{{ route('file.uuid',['url','products',$item->product->uuid,$item->product->images[0]->url]) }}"
                                                            alt="Product"></a>
                                            </td>
                                            <td class="cart_product_desc">
                                                <h5>{{ $item->product->name }}</h5>
                                            </td>
                                            <td class="price">
                                                <span>{!! currency($item->product->discount_price, 'USD', session('currency')) !!}</span>
                                            </td>
                                            <td>
                                                {{ $item->size }}
                                            </td>
                                            <td>
                                                {{ $item->color }}
                                            </td>
                                            <td class="qty">
                                                <div class="qty-btn d-flex">
                                                    <p>Qty</p>
                                                    <div class="quantity">
                                                    <span class="qty-minus" data-uuid="{{ $item->uuid }}"
                                                          onclick="var effect = document.getElementById('qty-{{ $key }}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;qtyModify(this);return false;"><i
                                                                class="fa fa-minus" aria-hidden="true"></i></span>
                                                        <input type="number" class="qty-text" id="qty-{{ $key }}"
                                                               step="1"
                                                               min="1" max="300" name="{{ $item->uuid }}"
                                                               value="{{ $item->qty }}">
                                                        <span class="qty-plus" data-uuid="{{ $item->uuid }}"
                                                              onclick="var effect = document.getElementById('qty-{{ $key }}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;qtyModify(this);return false;"><i
                                                                    class="fa fa-plus" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('cart.remove',$item->uuid) }}"><i
                                                            class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="2"
                                            style="width: 100%;flex:1;max-width:100%;text-align: center;margin: 0 auto">
                                            Card
                                            is empty!
                                        </td>
                                    </tr>
                                @endforelse
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="cart-summary">
                        <h5>Cart Total</h5>
                        <ul class="summary-table">
                            <li><span>subtotal:</span>
                                <span>{!! currency( $calculation->subtotal , 'USD', session('currency')) !!}</span></li>
                            <li><span>delivery:</span> <span>Free</span></li>
                            <li><span>discount:</span>
                                <span>-{!! currency( $calculation->discount , 'USD', session('currency')) !!}</span>
                            </li>
                            <li><span>total:</span>
                                <span>{!! currency( $calculation->total , 'USD', session('currency')) !!}</span></li>
                        </ul>
                        <div class="cart-btn mt-100">
                            <a href="{{ route('checkout') }}" class="btn amado-btn w-100">Checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        var timeout;

        function qtyModify(e) {
            var uuid = $(e).data('uuid');
            var qty = $('input[name="' + uuid + '"]').val();
            clearTimeout(timeout)
            timeout = setTimeout(function () {
                $.get('{{ url('') }}/cart/qty/' + uuid + '/' + qty, function () {
                    window.location.reload();
                })
            }, 1000)
        }
    </script>
@stop