@extends('web.layouts.main')
@section('content')
    <!-- Product Catagories Area Start -->
    <div class="products-catagories-area clearfix">
        <div class="amado-pro-catagory clearfix">

            @forelse($catergories as $category)
            <!-- Single Catagory -->
            <div class="single-products-catagory clearfix">
                <a href="{{ route('shop').'?cat_id='.$category->uuid }}">
                    <img src="{{ route('file',['url','categories',$category->image]) }}" alt="">
                    <!-- Hover Content -->
                    <div class="hover-content">
                        <div class="line"></div>
                        <p>From {!! currency($category->start_form, 'USD', session('currency')) !!}</p>
                        <h4>{{ $category->name }}</h4>
                    </div>
                </a>
            </div>
                @empty
                Categories are currently not available
            @endforelse
        </div>
    </div>
@stop