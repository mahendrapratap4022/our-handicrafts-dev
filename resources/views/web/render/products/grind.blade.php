<div class="row">
@forelse($products as $product)
    <!-- Single Product Area -->
        <div class="col-12 col-sm-6 col-md-12 col-xl-6">
            <div class="single-product-wrapper">
                <!-- Product Image -->
                @if($product->images->count() > 0)
                <div class="product-img">
                        @php($first_image = array_first($product->images))
                        @php($last_image = array_last($product->images))
                        <img src="{{ route('file.uuid',['url','products',$product->uuid,$first_image->url]) }}"
                             alt="">
                        <!-- Hover Thumb -->
                        {{--<img class="hover-img" src="{{ route('file',['url','products',$last_image->url]) }}" alt="">--}}

                    </div>
                @endif

            <!-- Product Description -->
                <div class="product-description d-flex align-items-center justify-content-between">
                    <!-- Product Meta Data -->
                    <div class="product-meta-data">
                        <div class="line"></div>
                        @if($product->original_price > $product->discount_price)
                            <p class="original-price">{!! currency($product->original_price, 'USD', session('currency')) !!}</p>
                            <p class="product-price">{!! currency($product->discount_price, 'USD', session('currency')) !!}</p>
                        @else
                            <p class="product-price"
                               style="margin-top: 15px!important;">{!! currency($product->discount_price, 'USD', session('currency')) !!}</p>
                        @endif
                        <a href="{{ route('product', $product->uuid) }}">
                            <h6>{{ $product->name }}</h6>
                        </a>
                    </div>
                    <!-- Ratings & Cart -->
                    <div class="ratings-cart text-right">
                        <div class="ratings">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <div class="cart">
                            <a href="{{ route('product', $product->uuid) }}"><img
                                        src="{{ asset('img/core-img/cart.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        Specified filter doesn't matched any products, please change the filters
    @endforelse
    {!! $products->links('web.pagination.store') !!}
</div>