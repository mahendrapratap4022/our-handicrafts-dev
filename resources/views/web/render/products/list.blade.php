<div class="row">
@forelse($products as $product)
    <!-- Single Product Area -->
        <div class="col-12">
            <div class="single-product-wrapper">
                <div class="row">
                    <div class="col-6">
                        <!-- Product Image -->
                        @if($product->images->count() > 0)
                            <div class="product-img">
                                @php($first_image = array_first($product->images))
                                @php($last_image = array_last($product->images))
                                <img src="{{ route('file.uuid',['url','products',$product->uuid,$first_image->url]) }}"
                                     alt="">
                                <!-- Hover Thumb -->
                                {{--<img class="hover-img" src="{{ route('file',['url','products',$last_image->url]) }}" alt="">--}}

                            </div>
                        @endif
                    </div>
                    <div class="col-6">


                        <a href="{{ route('product', $product->uuid) }}">
                            <h6>{{ $product->name }}</h6>
                        </a>
                        <!-- Product Description -->
                        <div class="product-description list">
                            <!-- Product Meta Data -->
                            <div class="product-meta-data">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="line"></div>
                                    </div>
                                    <div class="col-6 text-right">
                                        @if($product->original_price > $product->discount_price)
                                            <p class="original-price">{!! currency($product->original_price, 'USD', session('currency')) !!}</p>
                                            <p class="product-price">{!! currency($product->discount_price, 'USD', session('currency')) !!}</p>
                                        @else
                                            <p class="product-price"
                                               style="margin-top: 15px!important;">{!! currency($product->discount_price, 'USD', session('currency')) !!}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <p class="description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                            <!-- Ratings & Cart -->
                            <div class="ratings-cart text-right">
                                <div class="ratings">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="cart">
                                    <a href="{{ route('product', $product->uuid) }}"><img
                                                src="{{ asset('img/core-img/cart.png') }}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        Specified filter doesn't matched any products, please change the filters
    @endforelse
    {!! $products->links('web.pagination.store') !!}
</div>