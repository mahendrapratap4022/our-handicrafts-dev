
<!-- ##### Footer Area Start ##### -->
<footer class="footer_area clearfix">
    <div class="container">
        <div class="row align-items-center">
            <!-- Single Widget Area -->
            <div class="col-12 col-lg-4">
                <div class="single_widget_area">
                    <!-- Logo -->
                    <div class="footer-logo mr-50">
                        <a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt=""></a>
                    </div>
                    <!-- Copywrite Text -->
                    <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved by ourhandicraft.com
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
            </div>
            <!-- Single Widget Area -->
            <div class="col-12 col-lg-8">
                <div class="single_widget_area">
                    <!-- Footer Menu -->
                    <div class="footer_menu">
                        <nav class="navbar navbar-expand-lg justify-content-end">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#footerNavContent" aria-controls="footerNavContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                            <div class="collapse navbar-collapse" id="footerNavContent">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="{{ route('home') }}">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('shop') }}">Shop</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('about-us') }}">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('cart') }}">Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('faq') }}">Faq</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- ##### Footer Area End ##### -->

<!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
<script src="{{ asset('js/jquery/jquery-2.2.4.min.js') }}"></script>
<!-- Popper js -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
@yield('js')

<!-- Plugins js -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- Active js -->
<script src="{{ asset('js/active.js') }}"></script>

<!-- toaster button js -->
{!! Html::script('admin-assets/plugins/components/toast-master/js/jquery.toast.js') !!}
{!! Html::script('admin-assets/js/waves.js') !!}

<script>
    @if(Session::get('type')=='success')
    $.toast({
        heading: '{{Session::get('messageTitle')}}',
        text: '{!! Session::get('messageText') !!}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: '{{Session::get('type')}}',
        hideAfter: 3500,
        stack: 6
    });
    @elseif(Session::get('type')=='error')
    $.toast({
        heading: '{{Session::get('messageTitle')}}',
        text: '{!! Session::get('messageText') !!}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: '{{Session::get('type')}}',
        hideAfter: 14000
    });
    @endif
</script>
</body>
</html>