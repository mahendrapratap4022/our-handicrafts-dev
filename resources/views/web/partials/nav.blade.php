<!-- Mobile Nav (max width 767px)-->
<div class="mobile-nav">
    <!-- Navbar Brand -->
    <div class="amado-navbar-brand">
        <a href="{{route('home')}}"><img src="img/core-img/logo.png" alt=""></a>
    </div>
    <!-- Navbar Toggler -->
    <div class="amado-navbar-toggler">
        <span></span><span></span><span></span>
    </div>
</div>

<!-- Header Area Start -->
<header class="header-area clearfix">
    <!-- Close Icon -->
    <div class="nav-close">
        <i class="fa fa-close" aria-hidden="true"></i>
    </div>
    <!-- Logo -->
    <div class="logo">
        <a href="{{route('home')}}"><img src="{{ asset('img/core-img/logo.png') }}" alt=""></a>
    </div>
    <!-- Amado Nav -->
    <nav class="amado-nav">
        <ul>
            <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="{{route('home')}}">Home</a></li>
            <li class="{{ request()->is('shop') ? 'active' : '' }}"><a href="{{route('shop')}}">Shop</a></li>
            <li class="{{ request()->is('faq') ? 'active' : '' }}"><a href="{{route('faq')}}">FAQ</a></li>
            <li class="{{ request()->is('about-us') ? 'active' : '' }}"><a href="{{route('about-us')}}">About Us</a></li>
            <li class="{{ request()->is('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Contact</a></li>
        </ul>
    </nav>
    <!-- Button Group -->
    <div class="amado-btn-group mt-30 mb-100">
        <br>
        <form action="{{route('change-currency')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <select class="w-100" id="country" name="code" onchange="this.form.submit()">
                @foreach(currency()->getCurrencies() as $currency)
                    <option value="{{$currency['code']}}" {{$currency['code']==session('currency')?"selected":""}}>{{$currency['code']}} ( {{$currency['symbol']}} )</option>
                @endforeach
            </select>
        </form>
    </div>
    <!-- Cart Menu -->
    <div class="cart-fav-search mb-100">
        @php($calculation = new \App\Services\Calculation())
        <a href="{{route('cart')}}" class="cart-nav"><img src="img/core-img/cart.png" alt=""> Cart <span>({{ $calculation->product_count }})</span></a>
        <a href="#" class="search-nav"><img src="img/core-img/search.png" alt=""> Search</a>
    </div>
    <!-- Social Button -->
    <div class="social-info d-flex justify-content-between">
        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    </div>
</header>
<!-- Header Area End -->