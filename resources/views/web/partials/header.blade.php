<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="title" content="@yield('meta-title','Our HandiCrafts')">
    <meta name="description" content="@yield('meta-description','Best handiCrafts items, Best handiCrafts items in udaipur, Udaipur best  handiCrafts items')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>@yield('title','Our HandiCrafts')</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Toaster button css -->
{!! Html::style('admin-assets/plugins/components/toast-master/css/jquery.toast.css') !!}
    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{ asset('css/core-style.css') }}">
    <link rel="stylesheet" href="{{ asset('style.css') }}">

    @yield('header')

</head>

<body>