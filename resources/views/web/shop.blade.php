@extends('web.layouts.main')
@section('content')
    <!-- Product Catagories Area Start -->
    <div class="shop_sidebar_area">

        <!-- ##### Single Widget ##### -->
        <div class="widget catagory mb-50">
            <!-- Widget Title -->
            <h6 class="widget-title mb-30">Catagories</h6>

            <!--  Catagories  -->
            <div class="catagories-menu">
                <ul>
                    <li class="active"><a href="{{ route('shop') }}">All</a></li>
                    @forelse($categories as $category)
                        <li>
                            <a href="#" data-category="{{ $category->uuid }}"
                               onclick="changeCategory(this)">{{ $category->name }}</a>
                        </li>
                    @empty
                    @endforelse
                </ul>
            </div>
        </div>


        <!-- ##### Single Widget ##### -->
        <div class="widget color mb-50">
            <!-- Widget Title -->
            <h6 class="widget-title mb-30">Color</h6>

            <div class="widget-desc">
                <ul class="d-flex">
                    @foreach($colors as $color)
                        <li><a href="#" style="background-color:{{ $color }};color: white;text-align: center;"
                               data-color="{{ $color }}" onclick="changeColor(this)"><i class="fa fa-check"></i></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

    </div>

    <div class="amado_product_area section-padding-100">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="product-topbar d-xl-flex align-items-end justify-content-between">
                        <!-- Total Products -->
                        <div class="total-products">
                            <p id="entriesText"></p>
                            <div class="view d-flex">
                                <a href="#" class="active" data-view="grind" onclick="changeView(this)"><i
                                            class="fa fa-th-large" aria-hidden="true"></i></a>
                                <a href="#" data-view="list" onclick="changeView(this)"><i class="fa fa-bars"
                                                                                           aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <!-- Sorting -->
                        <div class="product-sorting d-flex">
                            <div class="sort-by-date d-flex align-items-center mr-15">
                                <p>Sort by</p>
                                <form action="#" method="get">
                                    <select name="select" id="sortBydate" onchange="changeSortBy(this)">
                                        <option value="0" selected>Newest</option>
                                        <option value="1">Old</option>
                                    </select>
                                </form>
                            </div>
                            <div class="view-product d-flex align-items-center">
                                <p>Size</p>
                                <form action="#" method="get">
                                    <select name="select" id="viewProduct" onchange="changeSize(this)">
                                        <option value="all">All</option>
                                        @foreach($sizes as $key => $size)
                                            <option value="{{ $size->name }}">{{ $size->name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="result" class="result"></div>

        </div>
    </div>
@stop
@section('header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('js')
    <script>
        var category;
        var colors;
        var sort_by;
        var size;
        var view;

        $(document).ready(function () {
            var array = JSON.parse('<?php echo json_encode($colors); ?>');
            colors = (array);
            sort_by = $('#sortBydate').val();
            size = $("#viewProduct").val();
            var cat_id = '{{ \Illuminate\Support\Facades\Input::get('cat_id') }}';
            if (cat_id) {
                $('.catagories-menu ul li.active').removeClass('active');
                $('.catagories-menu').find('[data-category="' + cat_id + '"]').parent().closest('li').addClass('active');
                category = cat_id
            }
            filter();
        });

        function changeCategory(e) {
            if ($(e).data('category') != category) {
                category = $(e).data('category');
                $('.catagories-menu ul li.active').removeClass('active');

                $(e).parent().closest('li').addClass('active');
                filter();
            }
        }

        function changeColor(e) {
            var selectedColor = $(e).data('color');
            console.log(colors.length);
            if ($.inArray(selectedColor, colors) != -1 && colors.length > 1) {
                colors.splice($.inArray(selectedColor, colors), 1);
                $(e).html('');
            } else if ($.inArray(selectedColor, colors) == -1) {
                colors.push(selectedColor)
                $(e).html('<i class="fa fa-check"></i>')
            }
            filter()
        }

        function changeSortBy(e) {
            sort_by = $(e).val();
            console.log(sort_by);
            filter()
        }

        function changeSize(e) {
            size = $(e).val();
            filter()
        }

        function changeView(e) {
            var selectedView = $(e);
            $('.total-products a').removeClass('active');
            selectedView.addClass('active');
            view = selectedView.data('view');
            filter()
        }

        function filter(page) {
            $('#loader').show();
            var url = '/shop/filter';
            var search = "{{ \Illuminate\Support\Facades\Input::get('search') }}";
            if (page) {
                url = 'shop/filter?page=' + page
            }
            var data = {
                category: category,
                colors: colors,
                sort_by: sort_by,
                size: size,
                view: view,
            };
            if (search) {
                data.search = search;
            }
            var csrf = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ url('') }}' + url,
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function (s) {
                    $('#result').html(s.html)
                    $('#entriesText').html(s.entriesText);
                    $('#loader').hide();
                }, error: function (e) {
                    console.log(e.responseText);
                }
            })
        }

        function addToCart(e) {
            var uuid = $(e).data('uuid');
            $.get('{{ url('') }}/cart/add/' + uuid, function (s) {
                $(e).attr('onclick', 'removeFromCart(this)');
                $(e).html('<i class="fa fa-check" style="color:black"></i>')
            })
        }

        function removeFromCart(e) {
            var uuid = $(e).data('uuid');
            $.get('{{ url('') }}/cart/remove/' + uuid, function (s) {
                $(e).attr('onclick', 'addToCart(this)');
                $(e).html('<img src="img/core-img/cart.png" alt="">'
                )
            })
        }
    </script>
@stop