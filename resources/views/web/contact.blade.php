@extends('web.layouts.main')
@section('content')
    <!-- Product Catagories Area Start -->
    <div class="cart-table-area section-padding-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="cart-title mt-50">
                        <h2>Contact</h2>
                    </div>
                </div>
            </div>
            <form id="contact-form" method="post" action="{{ route('contact') }}" role="form">
{{ csrf_field() }}

                <div class="controls">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="form_name">Full Name *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Jon doe" >
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="example@gmail.com">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                {{ $errors->first('email') }}
                            </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control" placeholder="Message" rows="10"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                {{ $errors->first('message') }}
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn amado-btn" value="Send message">
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@stop