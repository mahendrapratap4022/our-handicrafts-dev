@extends('web.layouts.main')
@section('content')
    <div class="single-product-area section-padding-100 clearfix">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <div class="exzoom" id="exzoom">
                        <!-- Images -->
                        <div class="exzoom_img_box">
                            <ul class='exzoom_img_ul'>
                                @forelse($product->images as $image)
                                    <li>
                                        <img src="{{ route('file.uuid',['url','zoom',$product->uuid,$image->url]) }}"/>
                                    </li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        <div class="exzoom_nav"></div>
                        <!-- Nav Buttons -->
                        <p class="exzoom_btn">
                            <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
                            <a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
                        </p>
                    </div>
                {{--<img class="xzoom" src="{{ route('file.uuid',['url','products',$product->uuid,$first_image->url]) }}" xoriginal="{{ route('file.uuid',['url','zoom',$product->uuid,$first_image->url]) }}" />--}}
                {{--<div class="xzoom-thumbs">--}}
                {{--@forelse($product->images as $image)--}}

                {{--<a href="{{ route('file.uuid',['url','zoom',$product->uuid,$image->url]) }}">--}}
                {{--<img class="xzoom-gallery" width="80" src="{{ route('file.uuid',['url','products',$product->uuid,$image->url]) }}"  xpreview="{{ route('file.uuid',['url','products',$product->uuid,$image->url]) }}">--}}
                {{--</a>--}}
                {{--@empty--}}
                {{--@endforelse--}}
                {{--</div>--}}
            </div>
            <div class="col-12 col-lg-5">
                <div class="single_product_desc">
                    <!-- Product Meta Data -->
                    <div class="product-meta-data">
                        <div class="line"></div>
                        @if($product->original_price > $product->discount_price)
                            <p class="original-price">{!! currency($product->original_price, 'USD', session('currency')) !!}</p>
                        @endif
                        <p class="product-price">{!! currency($product->discount_price, 'USD', session('currency')) !!}</p>
                        <a href="#" onclick="return false;">
                            <h6>{{ $product->name }}</h6>
                        </a>
                        <p class="avaibility"><i class="fa fa-circle"></i> In Stock</p>
                    </div>

                    <div class="short_overview my-5">
                        <p>{{ $product->description }}</p>
                    </div>

                    <!-- Add to Cart Form -->
                    <div class="cart clearfix">
                        <div class="row">
                            <div class="cart-btn col-md-6 d-flex mb-50">
                                <p>Qty</p>
                                <div class="quantity">
                                    <span class="qty-minus"
                                          onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i
                                                class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    <input type="number" class="qty-text" id="qty" step="1" min="1" max="300"
                                           name="quantity" value="1">
                                    <span class="qty-plus"
                                          onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i
                                                class="fa fa-caret-up" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div class="cart-btn col-md-6 d-flex mb-50">
                                <p>Size</p>
                                <div>
                                    {!! Form::select('size',$product->size_list,null,['class' => 'qty-text']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- ##### Single Widget ##### -->
                        <div class="widget color mb-50">
                            <!-- Widget Title -->
                            <div class="widget-desc">
                                <ul class="d-flex">
                                    @foreach($product->colors as $key => $color)
                                        @if($key == 0)
                                            <li>
                                                <a href="#"
                                                   style="background-color:{{ $color->hexa }};color: white;text-align: center;"
                                                   data-color="{{ $color->name }}" data-cheked="true"
                                                   onclick="changeColor(this)"><i
                                                            class="fa fa-check"></i></a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="#"
                                                   style="background-color:{{ $color->hexa }};color: white;text-align: center;"
                                                   data-color="{{ $color->hexa }}" data-cheked="false"
                                                   onclick="changeColor(this)"></a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <button type="submit" name="addtocart" value="5" class="btn amado-btn"
                                onclick="addToCart(this)" data-uuid="{{ $product->uuid }}">Add to cart
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('header')
    {!! Html::style('plugins/components/exzoom/css/jquery.exzoom.css') !!}

@stop
@section('js')
    {!! Html::script('plugins/components/exzoom/js/jquery.exzoom.js') !!}
    <script>
        $(function(){
            $("#exzoom").exzoom({

                // thumbnail nav options
                "navWidth": 60,
                "navHeight": 60,
                "navItemNum": 5,
                "navItemMargin": 7,
                "navBorder": 1,

                // autoplay
                "autoPlay": false
            });
        });


        function addToCart(e) {
            var qty = $('#qty').val();
            var color = $('ul').find('[data-cheked="true"]').data('color');
            var size = $('select[name="size"]').val();
            var uuid = $(e).data('uuid');

            $.ajax({
                url: '{{ url('') }}/cart/add',
                method: 'post',
                data: {
                    uuid: uuid,
                    qty: qty, size:
                    size, color:
                    color
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                success: function (s) {
                    window.location.replace('{{ route('shop') }}')
                }, error: function (e) {
                    console.log(e.responseText)
                }
            })

        }

        function changeColor(e) {
            if (!$(e).data('cheked')) {
                $('ul').find('[data-cheked="true"]').attr('data-cheked', false).html('');
                $(e).attr('data-cheked', true).html('<i class="fa fa-check"></i>');
            }
        }
    </script>
@stop