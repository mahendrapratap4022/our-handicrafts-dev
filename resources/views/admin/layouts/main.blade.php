@include('admin.partials.header')
<!-- Wrapper -->
<div id="wrapper">

{{-- {{ Inlcude nav menu }}--}}
@include('admin.partials.side-menu')

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <?php $url = Request::segments(); ?>
                    <h4 class="page-title">{{ count($url)>=2? $url[count($url)-2] : end($url) }}</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <?php $segments = ''; ?>
                        @php ($i = 1)
                        @foreach(Request::segments() as $segment)
                            @if($i <= 1)
                                <?php $segments .= '/' . $segment; ?>
                                @if($segment != 'dashboard')
                                    <li class="dw">
                                        <a href="{{Route('admin.dashboard')}}">Dashboard</a>
                                    </li>
                                @endif
                                <li class="active">
                                    {{ucfirst($segment)}}
                                </li>
                            @endif
                            @php ($i++)
                        @endforeach
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        @yield('content')
                    </div>
                    @yield('content2')
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> <?= date("Y") ?> &copy; ourhandicrafts.com</footer>
    </div>
    <!-- End Page Content -->

</div>
<!-- /#wrapper -->
@include('admin.partials.footer')
