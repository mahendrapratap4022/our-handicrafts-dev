<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<html>
<body>
@yield('content')
</body>
</html>