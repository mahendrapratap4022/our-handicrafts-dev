@extends('admin.layouts.main')

@section('content')
   <h5><label class="form-group text-uppercase text-info">Booking Request</label></h5>
   <div class="table-responsive">
      <table class="table table-hover po-admin-booking manage-u-table">
         <thead>
         <tr>
            <th width="70" class="text-center">#</th>
            <th>Txtid</th>
            <th>User Name</th>
            <th>User Contact</th>
            <th>Booked On</th>
            <th>Amount Paid</th>
            <th width="250">Booking Status</th>
            <th>Actions</th>
         </tr>
         </thead>
         <tbody>
         @forelse($bookings as $key => $currentBooking)
            @php($user = $currentBooking->guest)
            <tr>
               <td class="text-center">{{ $key+1 }}</td>
               <td>{{ $currentBooking->cart_random_id }}</td>
               <td>{{ $user->name }}
                  <br/><span class="text-muted"></span>{{ $user->city }}</td>

               <td>{{ $user->email }}
                  <br/><span class="text-muted">{{ $user->number }}</span></td>
               <td>{{ date("d M Y",strtotime($currentBooking->created_at)) }}
                  <br/><span class="text-muted">{{ date("h: i A",strtotime($currentBooking->created_at)) }}</span></td>
               <td>
                  <span class="fa fa-inr"></span> {{ $currentBooking->payment->net_amount_debit }}
                  <br/>
                  <div class="label label-table label-success">Total</div>
               </td>
               <td>
                     <label><span class=" @if($currentBooking->payment->status == 'Completed') text-success @else text-danger @endif">{{ ucwords($currentBooking->payment->status) }}</span></label>
               </td>
               <td class="text-nowrap">
                  <a href="{{ route('invoice-download',$currentBooking->id) }}" class="label label-table label-info" data-toggle="tooltip" data-original-title="Invoice"> <i class="ti-download"></i> Invoice</a>
                  <a href="#" data-toggle="modal" data-target="#{{ $currentBooking->id }}"> <i
                             class="fa fa-eye text-inverse m-r-10"></i> </a>
                  <a href="#" data-toggle="modal" data-target="#remark-{{ $currentBooking->id }}"> <i
                             class="fa fa-comment-o text-inverse m-r-10"></i> </a></td>
            </tr>
         @empty
            <td colspan="8" align="center">Currently there is not any booking.</td>
         @endforelse
         </tbody>
      </table>
   </div>
   @forelse($bookings    as $currentBooking)
      <!-- sample modal content -->
      <div id="remark-{{ $currentBooking->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="alert alert-info">
                  <h4 class="modal-title" id="myModalLabel">Booking Remarks</h4>
               </div>
               <div class="modal-body">
                  {{ $currentBooking->comment }}

                  @if(!$currentBooking->comment)
                     Remark not available
                  @endif
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
               su   </button>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
      </div>
      <!-- sample modal content -->

      <div id="{{ $currentBooking->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-body po-admin-invoice">
                  @include('admin.partials.invoice-structure')
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                     </button>
                  </div>
               </div>
               <!-- /.modal-content -->
            </div>
         </div>
      </div>
   @empty
   @endforelse
   {!! $bookings->links() !!}
@endsection