@extends('admin.layouts.main')
@section('content')
    <form action="{{ route('admin.create-color')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-body">
            <h3 class="box-title">Colors Info</h3>
            <hr class="m-t-0 m-b-10">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label">Color Name</label>
                        <input type="text" class="form-control" placeholder="Name of the category" required autocomplete="off"
                               name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                           {{ $errors->first('name') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('hexa') ? ' has-error' : '' }}">
                        <label class="control-label">Select Color</label><br>
                        <input type="text" class="colorpicker form-control" required autocomplete="off" name="hexa" value="{{ old('hexa') }}">
                        @if ($errors->has('hexa'))
                            <span class="help-block">
                           {{ $errors->first('hexa') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
            <a href="{{route('admin.categories')}}" class="btn btn-default">Cancel</a>
        </div>
    </form>
@stop
@section('content2')
    <div class="white-box">
        <div class="table-responsive">
            <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>color</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                @php ($id = 1)
                @foreach($colors as $color)
                    <tr>
                        <td>{{$id}}</td>
                        <td>{{$color->name}}</td>
                        <td>{{$color->hexa}}</td>
                        <td>
                            <button data="{{route('admin.color.delete',$color->id)}}" class="btn btn-danger btn-outline btn-circle m-r-5" onclick="confirmdelete('{{route('admin.color.delete',$color->id)}}')"><i class="ti-trash"></i></button>
                        </td>
                    </tr>
                    @php ($id++)
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="7">
                        <div class="text-right">
                            <ul class="pagination"></ul>
                        </div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
@section('header-plugin-css')
    <!--alerts CSS -->
    {!! Html::style('plugins/components/sweetalert/sweetalert.css') !!}

    {!! Html::style('plugins/components/jquery-asColorPicker-master/css/asColorPicker.css') !!}
@endsection
@section('js')
    <!-- Sweet-Alert  -->
    {!! Html::script('plugins/components/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('plugins/components/sweetalert/jquery.sweet-alert.custom.js') !!}

    <!-- Color Picker Plugin JavaScript -->
    {!! Html::script('plugins/components/jquery-asColorPicker-master/libs/jquery-asColor.js') !!}
    {!! Html::script('plugins/components/jquery-asColorPicker-master/libs/jquery-asGradient.js') !!}
    {!! Html::script('plugins/components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') !!}
    <script>
        function confirmdelete(url) {
            swal({
                    title: "Are you sure you want to Delete",
                    text: "Your will not be able to recover this color after delete!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    window.location.href = url;
                });
        }

        $(".colorpicker").asColorPicker();
    </script>
@stop