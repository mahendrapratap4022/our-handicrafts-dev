<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/x-icon" sizes="16x16" href="plugins/images/favicon.ico">
    <title>OUR HANDICRAFTS</title>

    <!-- Bootstrap Core CSS -->
    {{ Html::style('admin-assets/bootstrap/css/bootstrap.min.css') }}
    <!-- animation CSS -->
    {{ Html::style('admin-assets/css/animate.css') }}
    <!-- Menu CSS -->
    {{ Html::style('admin-assets/plugins/components/sidebar-nav/dist/sidebar-nav.min.css') }}
    <!-- Toaster button css -->
    {!! Html::style('admin-assets/plugins/components/toast-master/css/jquery.toast.css') !!}
    @yield('header-plugin-css')
    <!-- Custom CSS -->
    {{ Html::style('admin-assets/css/style.css') }}
    <!-- color CSS -->
    {{ Html::style('admin-assets/css/colors/default.css') }}
    {{ Html::style('admin-assets/css/colors/blue.css') }}


    <!-- Custom at our handicrafts end CSS -->
    {{ Html::style('admin-assets/css/global.css') }}
	<!-- Custom Responsive CSS -->
    {{ Html::style('admin-assets/css/responsive.css') }}

</head>
<body class="fix-header po-body">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>