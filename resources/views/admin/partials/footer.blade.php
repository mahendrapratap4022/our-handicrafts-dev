<!-- Scripts -->
<!-- jQuery -->
{!! Html::script('admin-assets/plugins/components/jquery/dist/jquery.min.js') !!}
<!-- Bootstrap Core JavaScript -->
{!! Html::script('admin-assets/bootstrap/js/bootstrap.min.js') !!}
<!-- Menu Plugin JavaScript -->
{!! Html::script('admin-assets/plugins/components/sidebar-nav/dist/sidebar-nav.min.js') !!}
<!--slimscroll JavaScript -->
{!! Html::script('admin-assets/js/jquery.slimscroll.js') !!}
<!--Wave Effects -->
@yield('js')
<!-- toaster button js -->
{!! Html::script('admin-assets/plugins/components/toast-master/js/jquery.toast.js') !!}
{!! Html::script('admin-assets/js/waves.js') !!}
<!-- Custom Theme JavaScript -->
{!! Html::script('admin-assets/js/custom.min.js') !!}

<script>
    @if(Session::get('type')=='success')
    $.toast({
        heading: '{{Session::get('messageTitle')}}',
        text: '{!! Session::get('messageText') !!}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: '{{Session::get('type')}}',
        hideAfter: 3500,
        stack: 6
    });
    @elseif(Session::get('type')=='error')
    $.toast({
        heading: '{{Session::get('messageTitle')}}',
        text: '{!! Session::get('messageText') !!}',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: '{{Session::get('type')}}',
        hideAfter: 14000
    });
    @endif
</script>

@yield('footer-js')
<!--Style Switcher -->
{!! Html::script('admin-assets/plugins/components/styleswitcher/jQuery.style.switcher.js') !!}

</body>
</html>
