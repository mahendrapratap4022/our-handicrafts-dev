<table class="table" style="margin: 0 auto;" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td style="border:0;border-bottom: 1px solid #DADFE4; padding: 10px 0;">
            <img src="{{ asset('images/logo.png') }}" alt="" height="80px"/>
        </td>
        <td align="right" style="border:0;border-bottom: 1px solid #DADFE4; padding: 10px 0;">
            <span style="color:#23B2F5;"><strong>INVOICE</strong></span><br>
            <span>{{ $currentBooking->order_id }}</span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table" style="margin:0;">
                <tbody>
                <tr style="border:0;">
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Customer Name</label>
                        <p>{{ $currentBooking->guest->name }}</p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Email</label>
                        <p>{{ $currentBooking->guest->email }}</p>
                    </td>

                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Contact</label>
                        <p>{{ $currentBooking->guest->number }}</p>
                    </td>

                </tr>

                <tr style="border:0;">
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Booking Total</label>
                        <p>Rs. {!! currency( $currentBooking->guest->cart->total_amount ,'USD','INR','!') !!} </p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Payment</label>
                        <p>Rs. {{ $currentBooking->payment->net_amount_debit }} </p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Payment Status</label>
                        <p>{{ $currentBooking->payment->status }}</p>
                    </td>
                </tr>

                <tr style="border:0;">
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">TXNID</label>
                        <p>{{ $currentBooking->order_id }} </p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Currency Type</label>
                        <p>{{ $currentBooking->payment->currency_type ?? 'Not Available' }} </p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Currency Amount</label>
                        <p> {!! $currentBooking->payment->currency_type ? currency( $currentBooking->guest->cart->total_amount ,'USD',$currentBooking->payment->currency_type ) : 'Not Available!' !!} </p>
                    </td>
                </tr>
                <tr>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Booking Date</label>
                        <p>{{ date('d M Y', strtotime($currentBooking->created_at)) }}</p>
                    </td>

                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Pin Code</label>
                        <p>{{ $currentBooking->guest->code }}</p>
                    </td>
                    <td width="33%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">City</label>
                        <p>{{ $currentBooking->guest->city }}</p>
                    </td>
                </tr>
                <tr>
                    <td width="30%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Country</label>
                        <p>{{ $currentBooking->guest->countryRelation->name}}</p>
                    </td>
                    <td width="30%" style="border:0;">
                        <label style="margin-bottom:0; text-transform: uppercase;">Address</label>
                        <p>{{ $currentBooking->guest->address }}</p>
                    </td>
                </tr>
                @if($currentBooking->payment->status == 'Failed')
                    <tr style="border:0;">
                        <td colspan="5" style="border:0;">
                            <label style="margin-bottom:0; text-transform: uppercase;">Reason Of Payment Fail</label>
                            <p>{{ $currentBooking->payment->data['error_Message'] }}</p>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"
            style="background:#23B2F5; padding: 5px; text-transform: uppercase; font-weight: bold; color: #FFF;">
            Details
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th class="text-right">Total</th>
                </tr>
                </thead>
                <tbody>
                @forelse($currentBooking->guest->cart->items as $key => $item)
                    <tr>
                        <td class="text-center">{{ $key + 1  }}</td>
                        <td>{{ $item->product->name }}</td>
                        <td>Rs. {!! currency($item->product->discount_price,'USD','INR','!') !!}</td>
                        <td>{{ $item->qty }}</td>
                        <td class="text-right">
                            Rs. {!! currency($item->total_price * $item->qty,'USD','INR','!')  !!}</td>
                    </tr>
                @empty
                @endforelse
                <tr>
                    <td style="" align="right" colspan="2"><h4
                                style="font-family:'Rubik', sans-serif;margin:0;">Total</h4></td>
                    <td style="color:#23B2F5;" align="right" colspan="3"><h4
                                style="font-family:'Rubik', sans-serif;margin:0;">
                            Rs. {!! currency( $currentBooking->guest->cart->total_amount,'USD','INR','!') !!}</h4></td>

                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3" style="color:#9B9B9B;" align="center">2018 © ourhandicraft.com</td>
                </tr>
                </tfoot>
            </table>
        </td>
    </tr>
    </tbody>
</table>
