@extends('admin.layouts.main')
@section('content')
    <form action="{{ route('admin.create-category')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-body">
            <h3 class="box-title">Category Info</h3>
            <hr class="m-t-0 m-b-40">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label">Category Name</label>
                        <input type="text" class="form-control" placeholder="Name of the category" required
                               name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                           {{ $errors->first('name') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('start_form') ? ' has-error' : '' }}">
                        <label class="control-label">Sart price from</label>
                        <input type="text" class="form-control" placeholder="Sart price from" required
                               name="start_form" value="{{ old('start_form') }}">
                        @if ($errors->has('start_form'))
                            <span class="help-block">
                           {{ $errors->first('start_form') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('end_to') ? ' has-error' : '' }}">
                        <label class="control-label">End price to</label>
                        <input type="text" class="form-control" placeholder="End price to" required
                               name="end_to" value="{{ old('end_to') }}">
                        @if ($errors->has('end_to'))
                            <span class="help-block">
                           {{ $errors->first('end_to') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="control-label">Category Status</label>
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio1" value="1" @if(old('status')) checked @endif>
                                    <label for="radio1">Active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio2" value="0" @if(old('status')) checked @endif>
                                    <label for="radio2">Deactive</label>
                                </div>
                            </label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="help-block">
                          {{ $errors->first('status') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                        <label class="control-label">Category banner image</label>
                        <div class="input-group">
                            <label class="input-group-btn">
								<span class="btn btn-inverse">
									Browse&hellip; <input type="file" style="display: none;" name="image">
								</span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if ($errors->has('image'))
                            <span class="help-block">
                          {{ $errors->first('image') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group
                        {{ $errors->has('category') ? ' has-error' : '' }}">
                        <label class="control-label">Select Parent Category</label>
                        {!! Form::select('category', $selectCategory , old('category'), [ 'class' => 'form-control' ]) !!}
                        @if ($errors->has('category'))
                            <span class="help-block">
                          {{ $errors->first('category') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label">Description</label>
                        {!! Form::textarea('description', old('description'), ['rows' => '5', 'class' => 'form-control']) !!}
                        @if ($errors->has('description'))
                            <span class="help-block">
                          {{ $errors->first('description') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
            <a href="{{route('admin.categories')}}" class="btn btn-default">Cancel</a>
        </div>
    </form>
@stop
@section('content2')
    <div class="white-box">
        <ul class="nav customtab2 nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> All categories {{count($allcategories)}}</span></a></li>
            <li role="presentation" class=""><a href="#trashedcategories" aria-controls="trashedcategories" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Trashed categories {{ $trashedcategories->count() }}</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="categories">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Is Active</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($id = 1)
                        @foreach($allcategories as $category)
                            <tr>
                                <td>{{$id}}</td>
                                <td>{{$category->parent_uuid!=null? "--".$category->name:$category->name}}</td>
                                <td>{!! $category->status?'<span class="label label-success">Activate</span>':'<span class="label label-info m-r-10">Deactivate</span>' !!}</td>
                                <td>
                                    <a href="{{route('admin.category.edit',$category->uuid)}}" class="btn btn-info btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Edit"><i class="ti-pencil-alt"></i></a>
                                    <a href="{{route('admin.category.trash',$category->uuid)}}" class="btn btn-primary btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Move To Trash"><i class="ti-trash"></i></a>
                                </td>
                            </tr>
                            @php ($id++)
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="trashedcategories">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Is Active</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($id = 1)
                        @if($trashedcategories->count())
                            @foreach($trashedcategories as $categories)
                                <tr>
                                    <td>{{$id}}</td>
                                    <td>{{$categories->parent_uuid!=null? "--".$categories->name:$categories->name }}</td>
                                    <td>{!! $categories->status?'<span class="label label-success">Activate</span>':'<span class="label label-info m-r-10">Deactivate</span>' !!}</td>
                                    <td>
                                        <a href="{{route('admin.category.restore',$categories->uuid)}}" class="btn btn-success btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Restore"> <i class="icon-reload"></i></a>
                                        <button data="{{route('admin.category.delete',$categories->uuid)}}" class="btn btn-danger btn-outline btn-circle m-r-5" onclick="confirmdelete('{{route('admin.category.delete',$categories->uuid)}}')"><i class="ti-trash"></i></button>
                                    </td>
                                </tr>
                                @php ($id++)
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">Empty Trashed</td>
                            </tr>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('header-plugin-css')
    <!--alerts CSS -->
    {!! Html::style('plugins/components/sweetalert/sweetalert.css') !!}
@endsection
@section('js')
    <!-- Sweet-Alert  -->
    {!! Html::script('plugins/components/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('plugins/components/sweetalert/jquery.sweet-alert.custom.js') !!}
    <script>
        document.getElementById("myForm").onkeypress = function (e) {
            var key = e.charCode || e.keyCode || 0;
            if (key == 13) {
                e.preventDefault();
            }
        }

        function confirmdelete(url) {
            swal({
                    title: "Are you sure you want to Delete",
                    text: "Your will not be able to recover this Category after delete!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    window.location.href = url;
                });
        }
    </script>
@stop