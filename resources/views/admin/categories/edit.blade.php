@extends('admin.layouts.main')
@section('content')
    <form action="{{ route('admin.update-category')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {!! Form::hidden('uuid',$category->uuid) !!}
        <div class="form-body">
            <h3 class="box-title">Category Info</h3>
            <hr class="m-t-0 m-b-40">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label">Category Name</label>
                        <input type="text" class="form-control" placeholder="Name of the category" required
                               name="name" value="{{ old('name',$category->name) }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                           {{ $errors->first('name') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('start_form') ? ' has-error' : '' }}">
                        <label class="control-label">Sart price from</label>
                        <input type="text" class="form-control" placeholder="Sart price from" required
                               name="start_form" value="{{ old('start_form',$category->start_form) }}">
                        @if ($errors->has('start_form'))
                            <span class="help-block">
                           {{ $errors->first('start_form') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('end_to') ? ' has-error' : '' }}">
                        <label class="control-label">End price to</label>
                        <input type="text" class="form-control" placeholder="End price to" required
                               name="end_to" value="{{ old('end_to',$category->end_to) }}">
                        @if ($errors->has('end_to'))
                            <span class="help-block">
                           {{ $errors->first('end_to') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="control-label">Category Status</label>
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio1" value="1" @if(old('status',$category->status) == '1') checked @endif>
                                    <label for="radio1">Active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio2" value="0" @if(old('status',$category->status) == '0') checked @endif>
                                    <label for="radio2">Deactive</label>
                                </div>
                            </label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="help-block">
                          {{ $errors->first('status') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                        <label class="control-label">Category banner image</label>
                        <div class="input-group">
                            <label class="input-group-btn">
								<span class="btn btn-inverse">
									Browse&hellip; <input type="file" style="display: none;" name="image">
								</span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if ($errors->has('image'))
                            <span class="help-block">
                          {{ $errors->first('image') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group
                        {{ $errors->has('category') ? ' has-error' : '' }}">
                        <label class="control-label">Select Parent Category</label>
                        {!! Form::select('category', $selectCategory , old('category',$category->parent_uuid), [ 'class' => 'form-control' ]) !!}
                        @if ($errors->has('category'))
                            <span class="help-block">
                          {{ $errors->first('category') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label">Description</label>
                        {!! Form::textarea('description', old('description',$category->description), ['rows' => '5', 'class' => 'form-control']) !!}
                        @if ($errors->has('description'))
                            <span class="help-block">
                          {{ $errors->first('description') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
            <a href="{{route('admin.categories')}}" class="btn btn-default">Cancel</a>
        </div>
    </form>
@stop