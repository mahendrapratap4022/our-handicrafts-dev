@extends('admin.layouts.main')
@section('content')
    {{--<form action="{{ route('admin.user-carts.items')}}" method="post" enctype="multipart/form-data">--}}
        {{--{{ csrf_field() }}--}}
        {{--<div class="form-body">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4">--}}
                    {{--<div class="form-group {{ $errors->has('cart_id') ? ' has-error' : '' }}">--}}
                        {{--<label class="control-label">Cart Id</label>--}}
                        {{--<input type="text" class="form-control" name="cart_id" placeholder="264546" required>--}}
                        {{--@if ($errors->has('cart_id'))--}}
                            {{--<span class="help-block">--}}
                           {{--{{ $errors->first('cart_id') }}--}}
                      {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-actions">--}}
            {{--<button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Submit</button>--}}
        {{--</div>--}}
    {{--</form>--}}
    <div class="white-box">
        <ul class="nav customtab2 nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Cart Items {{count($cart->items)}}</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="categories">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Qty</th>
                            <th>Size</th>
                            <th>Color</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($id = 1)
                        @forelse($cart->items as $item)
                            <tr>
                                <td>{{$id}}</td>
                                <td>{{$item->product->name}}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->size }}</td>
                                <td>{{ $item->color }}</td>
                            </tr>
                            @php($id++)
                            @empty
                            Cart is empty!
                        @endforelse
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop