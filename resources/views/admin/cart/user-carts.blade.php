@extends('admin.layouts.main')
@section('content')
   <form action="{{ route('admin.user-carts.items')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="form-body">
         <h3 class="box-title">User Carts</h3>
         <hr class="m-t-0 m-b-40">
         <div class="row">
            <div class="col-md-4">
               <div class="form-group {{ $errors->has('cart_id') ? ' has-error' : '' }}">
                  <label class="control-label">Cart Id</label>
                  <input type="text" class="form-control" name="cart_id" placeholder="264546" required>
                  @if ($errors->has('cart_id'))
                     <span class="help-block">
                           {{ $errors->first('cart_id') }}
                      </span>
                  @endif
               </div>
            </div>
         </div>
      </div>

      <div class="form-actions">
         <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Submit</button>
      </div>
   </form>
@stop