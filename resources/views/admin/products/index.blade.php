@extends('admin.layouts.main')
@section('content')
    <form action="{{ route('admin.create-product')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-body">
            <h3 class="box-title">Product Info</h3>
            <hr class="m-t-0 m-b-40">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label">Product Name</label>
                        <input type="text" class="form-control" placeholder="Name of the Product" required
                               name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                           {{ $errors->first('name') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="control-label">Product Status</label>
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info m-t-5 {{old('status')}}">
                                    <input type="radio" name="status" id="radio1" value="1" @if(old('status')===1) checked @endif @if(old('status')===null) checked @endif>
                                    <label for="radio1">Active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio2" value="0" @if(old('status')===0) checked @endif>
                                    <label for="radio2">Deactive</label>
                                </div>
                            </label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="help-block">
                          {{ $errors->first('status') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('original_price') ? ' has-error' : '' }}">
                        <label class="control-label">Original Price</label>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </span>
                            <input type="text" name="original_price" class="form-control" placeholder="Original Price" name="original_price" value="{{ old('original_price') }}">
                        </div>
                        @if ($errors->has('original_price'))
                            <span class="help-block">
                            {{ $errors->first('original_price') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('discount_price') ? ' has-error' : '' }}">
                        <label class="control-label">Discount Price</label>
                        <div class="input-group {{ $errors->has('discount_price') ? ' has-error' : '' }}">
                         <span class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </span>
                            <input type="text" class="form-control" placeholder="Discount Price" required
                                   name="discount_price" value="{{ old('discount_price') }}">
                        </div>
                        @if ($errors->has('discount_price'))
                            <span class="help-block">
                           {{ $errors->first('discount_price') }}
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
                        <label class="control-label">Product images</label>
                        <div class="input-group">
                            <label class="input-group-btn">
								<span class="btn btn-inverse">
									Browse&hellip; <input type="file" style="display: none;" name="images[]" multiple>
								</span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if ($errors->has('images'))
                            <span class="help-block">
                          {{ $errors->first('images') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group
                        {{ $errors->has('category_uuid') ? ' has-error' : '' }}">
                        <label class="control-label">Select Parent Category</label>
                        {!! Form::select('category_uuid', $categories , old('category_uuid'), [ 'class' => 'form-control', 'autocomplete'=>"off"]) !!}
                        @if ($errors->has('category_uuid'))
                            <span class="help-block">
                                {{ $errors->first('category_uuid') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('colors') ? ' has-error' : '' }}">
                        <label class="control-label">Product color</label>
                        {!! Form::select('colors[]', $colors , old('colors'), ['multiple'=>"multiple", 'autocomplete'=>"off" , 'class' => 'select2 m-b-10 select2-multiple' ]) !!}
                        @if ($errors->has('colors'))
                            <span class="help-block">
                                {{ $errors->first('colors') }}
                             </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('sizes') ? ' has-error' : '' }}">
                        <label class="control-label">Product Size</label>
                        {!! Form::select('sizes[]', $sizes , old('sizes'), ['multiple'=>"multiple","autocomplete"=>"off" , 'class' => 'select2 m-b-10 select2-multiple' ]) !!}
                        @if ($errors->has('sizes'))
                            <span class="help-block">
                                {{ $errors->first('sizes') }}
                             </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label">Description</label>
                        {!! Form::textarea('description', old('description'), ['rows' => '5', 'class' => 'form-control']) !!}
                        @if ($errors->has('description'))
                            <span class="help-block">
                                {{ $errors->first('description') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
            <a href="{{route('admin.products')}}" class="btn btn-default">Cancel</a>
        </div>
    </form>
@stop
@section('content2')
    <div class="white-box">
        <ul class="nav customtab2 nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> All Products {{count($allproducts)}}</span></a></li>
            <li role="presentation" class=""><a href="#trashedproducts" aria-controls="trashedproducts" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Trashed Products {{ $trashedproducts->count() }}</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="products">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Is Active</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($id = 1)
                        @foreach($allproducts as $product)
                            <tr>
                                <td>{{$id}}</td>
                                <td>{{$product->name}}</td>
                                <td>{!! $product->status?'<span class="label label-success">Activate</span>':'<span class="label label-info m-r-10">Deactivate</span>' !!}</td>
                                <td>
                                    <a href="{{route('admin.product.edit',$product->uuid)}}" class="btn btn-info btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Edit"><i class="ti-pencil-alt"></i></a>
                                    <a href="{{route('admin.product.trash',$product->uuid)}}" class="btn btn-primary btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Move To Trash"><i class="ti-trash"></i></a>
                                </td>
                            </tr>
                            @php ($id++)
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="trashedproducts">
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Is Active</th>
                            <th>Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php ($id = 1)
                        @if($trashedproducts->count())
                            @foreach($trashedproducts as $product)
                                <tr>
                                    <td>{{$id}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{!! $product->status?'<span class="label label-success">Activate</span>':'<span class="label label-info m-r-10">Deactivate</span>' !!}</td>
                                    <td>
                                        <a href="{{route('admin.product.restore',$product->uuid)}}" class="btn btn-success btn-outline btn-circle m-r-5" data-toggle="tooltip" data-original-title="Restore"> <i class="icon-reload"></i></a>
                                        <button data="{{route('admin.product.delete',$product->uuid)}}" class="btn btn-danger btn-outline btn-circle m-r-5" onclick="confirmdelete('{{route('admin.product.delete',$product->uuid)}}')"><i class="ti-trash"></i></button>
                                    </td>
                                </tr>
                                @php ($id++)
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">Empty Trashed</td>
                            </tr>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('header-plugin-css')
    <!--alerts CSS -->
    {!! Html::style('plugins/components/sweetalert/sweetalert.css') !!}

    {!! Html::style('plugins/components/custom-select/custom-select.css') !!}
    {!! Html::style('plugins/components/bootstrap-select/bootstrap-select.min.css') !!}
    {!! Html::style('plugins/components/multiselect/css/multi-select.css') !!}
@endsection
@section('js')
    <!-- Sweet-Alert  -->
    {!! Html::script('plugins/components/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('plugins/components/sweetalert/jquery.sweet-alert.custom.js') !!}

    {!! Html::script('plugins/components/custom-select/custom-select.min.js') !!}
    {!! Html::script('plugins/components/bootstrap-select/bootstrap-select.min.js') !!}
    {!! Html::script('plugins/components/multiselect/js/jquery.multi-select.js') !!}
    <script>
        // For select 2
        $(".select2").select2();

        function confirmdelete(url) {
            swal({
                    title: "Are you sure you want to Delete",
                    text: "Your will not be able to recover this Category after delete!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    window.location.href = url;
                });
        }
    </script>
@stop