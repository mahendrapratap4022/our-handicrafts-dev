@extends('admin.layouts.main')
@section('content')
    <br>
    <button type="button" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-primary pull-right">Product Images</button>
    <br>
    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Product Images</h4></div>
                <div class="modal-body">
                    <div class="table-responsive">
                        {{Form::open(['method' => 'post', 'route' => 'admin.product-images-delete'])}}
                        {{ csrf_field() }}
                        {!! Form::hidden('uuid',$product->uuid) !!}
                        @if($product->images->count())
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete Selected Images</button>
                            <br>
                            <br>
                        @endif
                        <table class="table color-table info-table">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="checkAll"></th>
                                <th>Image</th>
                                <th>Download</th>
                                <th>Created on</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($product->images->count())
                                @foreach($product->images as $image)
                                    <tr>
                                        <td><input type="checkbox" id="checkItem" name="image_id[]" value="{{$image->id}}"></td>
                                        <td><img src="{{ route('file.uuid',['url','products',$product->uuid,$image->url]) }}" alt="" class="img-responsive" style="width: 100px;"></td>
                                        <td><a href="{{ route('file.uuid',['download','products',$product->uuid,$image->url]) }}">{{$image->url}}</a></td>
                                        <td>{{$image->created_at->diffForHumans()}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">There is no Images Available.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <form action="{{ route('admin.update-product')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {!! Form::hidden('uuid',$product->uuid) !!}
        <div class="form-body">
            <h3 class="box-title">Product Info</h3>
            <hr class="m-t-0 m-b-40">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label">Product Name</label>
                        <input type="text" class="form-control" productholder="Name of the Product" required
                               name="name" value="{{ old('name',$product->name) }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="control-label">Product Status</label>
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info m-t-5 {{old('status')}}">
                                    <input type="radio" name="status" id="radio1" value="1" @if(old('status',$product->status)===1) checked @endif @if(old('status',$product->status)===null) checked @endif>
                                    <label for="radio1">Active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info m-t-5">
                                    <input type="radio" name="status" id="radio2" value="0" @if(old('status',$product->status)===0) checked @endif>
                                    <label for="radio2">Deactive</label>
                                </div>
                            </label>
                        </div>
                        @if ($errors->has('status'))
                            <span class="help-block">
                          {{ $errors->first('status') }}
                      </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('original_price') ? ' has-error' : '' }}">
                        <label class="control-label">Original Price</label>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </span>
                            <input type="text" name="original_price" class="form-control" productholder="Original Price" name="original_price" value="{{ old('original_price',$product->original_price) }}">
                        </div>
                        @if ($errors->has('original_price'))
                            <span class="help-block">
                            {{ $errors->first('original_price') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('discount_price') ? ' has-error' : '' }}">
                        <label class="control-label">Discount Price</label>
                        <div class="input-group {{ $errors->has('discount_price') ? ' has-error' : '' }}">
                         <span class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </span>
                            <input type="text" class="form-control" productholder="Discount Price" required
                                   name="discount_price" value="{{ old('discount_price',$product->discount_price) }}">
                        </div>
                        @if ($errors->has('discount_price'))
                            <span class="help-block">
                           {{ $errors->first('discount_price') }}
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
                        <label class="control-label">Product images</label>
                        <div class="input-group">
                            <label class="input-group-btn">
								<span class="btn btn-inverse">
									Browse&hellip; <input type="file" style="display: none;" name="images[]" multiple>
								</span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if ($errors->has('images'))
                            <span class="help-block">
                          {{ $errors->first('images') }}
                      </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group
                        {{ $errors->has('category_uuid') ? ' has-error' : '' }}">
                        <label class="control-label">Select Parent Category</label>
                        {!! Form::select('category_uuid', $categories , old('category_uuid',$product->category_uuid), [ 'class' => 'form-control', 'autocomplete'=>"off"]) !!}
                        @if ($errors->has('category_uuid'))
                            <span class="help-block">
                                {{ $errors->first('category_uuid') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('colors') ? ' has-error' : '' }}">
                        <label class="control-label">Product color</label>
                        {!! Form::select('colors[]', $colors , old('colors',$product->colors), ['multiple'=>"multiple", 'autocomplete'=>"off" , 'class' => 'select2 m-b-10 select2-multiple' ]) !!}
                        @if ($errors->has('colors'))
                            <span class="help-block">
                                {{ $errors->first('colors') }}
                             </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('sizes') ? ' has-error' : '' }}">
                        <label class="control-label">Product Size</label>
                        {!! Form::select('sizes[]', $sizes , old('sizes',$product->sizes), ['multiple'=>"multiple","autocomplete"=>"off" , 'class' => 'select2 m-b-10 select2-multiple' ]) !!}
                        @if ($errors->has('sizes'))
                            <span class="help-block">
                                {{ $errors->first('sizes') }}
                             </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label">Description</label>
                        {!! Form::textarea('description', old('description',$product->description), ['rows' => '5', 'class' => 'form-control']) !!}
                        @if ($errors->has('description'))
                            <span class="help-block">
                                {{ $errors->first('description') }}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
            <a href="{{route('admin.products')}}" class="btn btn-default">Cancel</a>
        </div>
    </form>
@stop
@section('header-plugin-css')
    <!--alerts CSS -->
    {!! Html::style('plugins/components/sweetalert/sweetalert.css') !!}

    {!! Html::style('plugins/components/custom-select/custom-select.css') !!}
    {!! Html::style('plugins/components/bootstrap-select/bootstrap-select.min.css') !!}
    {!! Html::style('plugins/components/multiselect/css/multi-select.css') !!}
@endsection
@section('js')
    <!-- Sweet-Alert  -->
    {!! Html::script('plugins/components/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('plugins/components/sweetalert/jquery.sweet-alert.custom.js') !!}

    {!! Html::script('plugins/components/custom-select/custom-select.min.js') !!}
    {!! Html::script('plugins/components/bootstrap-select/bootstrap-select.min.js') !!}
    {!! Html::script('plugins/components/multiselect/js/jquery.multi-select.js') !!}
    <script>
        // For select 2
        $(".select2").select2();

        function confirmdelete(url) {
            swal({
                    title: "Are you sure you want to Delete",
                    text: "Your will not be able to recover this Category after delete!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    window.location.href = url;
                });
        }
        $(function () {
            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function () {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().reProduct(/\\/g, '/').reProduct(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready(function () {
                $(':file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log) alert(log);
                    }
                });
            });
        });
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@stop