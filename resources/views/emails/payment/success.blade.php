@component('mail::message')
Your Payment is successful. Please download your invoice from bellow link.

@component('mail::button', ['url' => $url])
Download
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
